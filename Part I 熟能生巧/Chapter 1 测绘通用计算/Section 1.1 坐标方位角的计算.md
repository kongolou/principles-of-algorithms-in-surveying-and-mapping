在外业观测中，我们常用极坐标法测量坐标方位角；在内业计算时，我们要用直角坐标反算坐标方位角。

下面讨论这两种情形下坐标方位角的计算问题。

# 极坐标情形

极坐标情形下，一条未知边的坐标方位角 $\alpha_k$ 是通过前一条已知边的坐标方位角 $\alpha_{k-1}$ 和转角 $\beta_k$ 计算得到的。具体有如下公式。

$$
\alpha_{k}=(\alpha_{k-1}\pm\beta_{k}-180\degree) \bmod 360\degree
$$

以路线前进方向为参考方向，当转角 $\beta_k$ 为左角时，其前面的符号取正；当转角 $\beta_k$ 为右角时，其前面的符号取负——即“左加右减”原则。大多数时候，我们所测的转角都是左角。

## 取模运算

注意到公式中存在一个特殊的运算符 $\bmod$ ，其定义如下。

$$
a \bmod b = a - b \times \left\lfloor \frac{a}{b} \right\rfloor
$$

上式的运算称为取模运算。取模运算可以将运算结果封闭在特定的区间内。比如，在极坐标情形下计算坐标方位角的公式中，它可以将运算所得的角度封闭在 $[\ 0\degree,\ 360\degree)$ 的区间内，或者说，映射到 $[\ 0\degree,\ 360\degree)$ 的集合中。

取模运算定义中的取整方式为向下取整，这一点与取余运算有所不同——取余运算定义中的取整方式为向零取整。

取余运算符 $\textrm{rem}$ 的定义如下。

$$
a \ \textrm{rem}\ b = a - b \times \left[ \frac{a}{b} \right]
$$

当 $\dfrac{a}{b}$ 的结果非负时，这两种运算等价；否则，两者将产生各自的结果。例如，在 $a=-60\degree,\ b=360\degree$ 时， $a \bmod b = 300\degree$ 而 $a \ \text{rem}\ b = -60\degree$。

我们当然不希望坐标方位角是负数。

# 直角坐标情形

直角坐标情形下坐标方位角的计算又称为坐标反算坐标方位角。

我们知道坐标正算的公式为

$$
\left\lbrace
\begin{aligned}
	x_{K} &= x_{O} + S_{OK} \cos{\alpha_{OK}} \\
	y_{K} &= y_{O} + S_{OK} \sin{\alpha_{OK}}
\end{aligned}
\right.
$$

那么坐标反算方位角的公式或许是

$$
\alpha_{OK}=\arctan{\frac{y_{K}-y_{O}}{x_{K}-x_{O}}}
$$

但是，我们细心观察会发现，在上述反算坐标方位角的公式中存在着一些问题：

1. 反三角函数 $\arctan()$ 的值域为 $(-90\degree,\ 90\degree)$ ，但是坐标方位角的定义域为 $[\ 0\degree,\ 360\degree)$ ;
2. 当 $x_{K}=x_{O}$ 时，分母为零。

看来反算坐标方位角并没有我们想的那么简单。

## 分类讨论

接下来，我们通过分类讨论来得出反算坐标方位角的公式。

![坐标方位角反算公式分类讨论](Fig1.1.png)

不难发现，坐标方位角的计算只与坐标增量有关。

不妨设

$$
\alpha_{OK}=\alpha(x_O,\ y_O,\ x_K,\ y_K)=\alpha_2(\Delta{y_{OK}},\ \Delta{x_{OK}})
$$

坐标增量的取值只有正、负和零三种情况。所以反算坐标方位角共有 $C_3^1 \times C_3^1=9$ 种情况。

1. 当 K 点位于 x 正半轴时， $\alpha_{OK}=\alpha_2(0,\ +)=0\degree$ 
2. 当 K 点位于第一象限时， $\alpha_{OK}=\alpha_2(+,\ +)=\angle{1}$ 
3. 当 K 点位于 y 正半轴时， $\alpha_{OK}=\alpha_2(+,\ 0)=90\degree$ 
4. 当 K 点位于第二象限时， $\alpha_{OK}=\alpha_2(+,\ -)=\angle{2}$ 
5. 当 K 点位于 x 负半轴时， $\alpha_{OK}=\alpha_2(0,\ -)=180\degree$ 
6. 当 K 点位于第三象限时， $\alpha_{OK}=\alpha_2(-,\ -)=\angle{3}$ 
7. 当 K 点位于 y 负半轴时， $\alpha_{OK}=\alpha_2(-,\ 0)=270\degree$ 
8. 当 K 点位于第四象限时， $\alpha_{OK}=\alpha_2(-,\ +)=\angle{4}$ 
9. 当 K 点与 O 点重合时， $\alpha_{OK}=\alpha_2(0,\ 0)=\textrm{undefined}$ 

  实际上最后一种情况几乎不会出现，不过我们依旧补充定义 $\alpha_2(0,\ 0)=0\degree$ 。

在以上讨论中， $\angle{1}$ 、 $\angle{2}$ 、 $\angle{3}$ 、 $\angle{4}$ 的计算方式如下。

$$
\begin{aligned}
	\angle{1} &= \arctan{\frac{\Delta{y_{OK}}}{\Delta{x_{OK}}}} \\
	\angle{2} &= \arctan{\frac{\Delta{y_{OK}}}{\Delta{x_{OK}}}} + 180\degree \\
	\angle{3} &= \arctan{\frac{\Delta{y_{OK}}}{\Delta{x_{OK}}}} + 180\degree \\
	\angle{4} &= \arctan{\frac{\Delta{y_{OK}}}{\Delta{x_{OK}}}} + 360\degree
\end{aligned}
$$

引入取模运算，算式将简化为

$$
\begin{aligned}
	\angle{1} = \angle{4} &= \arctan{\frac{\Delta{y_{OK}}}{\Delta{x_{OK}}}} \bmod 360\degree \\
	\angle{2} = \angle{3} &= \arctan{\frac{\Delta{y_{OK}}}{\Delta{x_{OK}}}} + 180\degree
\end{aligned}
$$

这启示我们可以换个角度理解这个问题。

原本 $\arctan()$ 函数的值域为 $(-90\degree,\ 90\degree)$ 。一方面，取模运算可以将其封闭在 $[\ 0\degree,\ 90\degree) \cup (270\degree,\ 360\degree)$ 中；另一方面，在加上 $180\degree$ （一个周期）后，值域变为 $(90\degree,\ 270\degree)$ 。最后再考虑边界情况，从而完成全部讨论。

综上所述，直角坐标情形下反算坐标方位角的公式如下。

$$
\alpha_{OK}=
\left\lbrace
\begin{aligned}
	\arctan{\frac{\Delta{y_{OK}}}{\Delta{x_{OK}}}} \bmod 360\degree, &\quad \Delta{x_{OK}} > 0 \\
	\arctan{\frac{\Delta{y_{OK}}}{\Delta{x_{OK}}}} + 180\degree, &\quad \Delta{x_{OK}} < 0 \\
	90\degree, &\quad \Delta{x_{OK}} = 0 \wedge \Delta{y_{OK}} > 0 \\
	270\degree, &\quad \Delta{x_{OK}} = 0 \wedge \Delta{y_{OK}} < 0 \\
	0\degree, &\quad \Delta{x_{OK}} = 0 \wedge \Delta{y_{OK}} = 0
\end{aligned}
\right.
$$

# 编程实现

在大多数编程语言中，`%` 是取余运算符（在 Python 语言中，`%` 为取模运算符）。

在 C++ 中，我们可以通过自定义一个函数来实现真正的取模运算。

```c++
// 取模运算
double mod(double a, double b) {
  return a - b * floor(a / b);
}
```

接下来，让我们完成两种情形下坐标方位角的计算。

```c++
// TODO: #include <cmath>
// 极坐标情形下坐标方位角的计算
double calc_alpha(double alpha, double beta, bool is_left = true) {
  double res = alpha;
  if (is_left)
    res += beta;
  else
    res -= beta;
  res -= M_PI;
  return mod(res, 2 * M_PI);
}
// 直角坐标情形下坐标方位角的计算（重载）
double calc_alpha(double x1, double y1, double x2, double y2) {
  double dx = x2 - x1;
  double dy = y2 - y1;
  double res = 0.0;
  if (dx > 0.0)
    res = mod(atan(dy / dx), 2 * M_PI);
  else if (dx < 0.0)
    res = atan(dy / dx) + M_PI;
  else if (dy > 0.0)
    res = M_PI / 2;
  else if (dy < 0.0)
    res = 3 * M_PI / 2;
  else
    ;
  return res;
}
```

值得注意的是，在编程时，我们应总是使用弧度进行角度运算，同时要尽力避免浮点数判零。
## 简洁形式

在大多数编程语言的数学库中，都提供了 `atan2()` 这样一个函数，将 `atan()` 函数的值域扩展到了  $(-180\degree,\ 180\degree]$ （两个周期），其本质与本节中定义的 $\alpha_2$ 函数（值域为 $[\ 0,\ 360\degree)$ 两个周期）类似。因此直角坐标情形下计算坐标方位角的函数得以写成如下简洁形式。

```c++
// TODO: #include <cmath>
// 直角坐标情形下坐标方位角的计算（第二种形式）
double calc_alpha2(double x1, double y1, double x2, double y2) {
  return mod(atan2(y2 - y1, x2 - x1), 2 * M_PI);
}
```

直角坐标情形下，这两种计算坐标方位角的方法如下图所示。

![反算坐标方位角的两种途径](Fig1.2.png)

此时，我们已经完成了测绘通用计算函数库中坐标方位角的部分。对于函数库中的其他部分，读者可以参阅附录 A 进一步了解。

[目录](./../../README.md)
