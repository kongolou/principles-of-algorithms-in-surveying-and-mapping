在上一节二维坐标情形中所展示的导线其实是坐标附合导线，与我们所熟知的附合导线略有不同。

那么，导线的类型有哪些？

在讨论之前，我们先作如下定义。

<dl>
  <dt>起点边（起始边或起算边）</dt>
  <dd>由路线起点指向测站起点的向量</dd>
  <dt>终点边</dt>
  <dd>由测站终点指向路线终点的向量</dd>
</dl>

# 起点边类型

起点边坐标方位角如何确定呢？最理想的方法是通过坐标反算得到。所以在常规导线中，起点边往往已知。

实际上，坐标方位角还可以用一种特殊的测量方式得到。而在有些时候，坐标北方向显得不那么重要，这时我们就可以假定起点边坐标方位角。

总而言之，起点边可以分为以下两种类型，它不影响最后导线的类型。

![导线起点边类型](Fig1.4.png)

# 终点边类型

真正影响导线类型的决定性因素是终点边类型。

根据终点边类型的不同，导线可以分为以下几种类型。

<dl>
  <dt>常规附闭合导线</dt>
  <dd>终点边已知（测站终点和路线终点的坐标均已知）</dd>
  <dt>坐标附闭合导线</dt>
  <dd>终点边测站终点坐标未知，而路线终点坐标已知</dd>
  <dt>方向附闭合导线</dt>
  <dd>终点边测站终点和路线终点的坐标均未知，但终点边的坐标方位角已知</dd>
  <dt>支导线</dt>
  <dd>终点边未知（仅有测站终点，无路线终点，且测站终点坐标未知）</dd>
</dl>

![导线终点边类型](Fig1.5.png)

值得注意的是，在常规附闭合导线中，终点边已知，这意味着我们能够增加终点边坐标方位角的检核条件。

$$
\alpha_{n} \equiv \alpha(x_n,\ y_n,\ x_B,\ y_B) \\
$$

[目录](./../../README.md)
