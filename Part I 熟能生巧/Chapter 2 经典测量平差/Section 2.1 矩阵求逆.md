经典测量平差常涉及大量的矩阵运算，让我们先来解决矩阵求逆的问题。

# 矩阵可逆的充要条件

如果要对一个矩阵求逆，首先要判断其是否可逆。

矩阵 $\mathbf{A}_n$ 可逆与以下命题等价：

1. $|\mathbf{A}_n| \ne 0$ （矩阵的行列式不等于零）
2. $\text{rank}(\mathbf{A}_n) = n$ （矩阵满秩）
3. $\mathbf{A_n} = \sum\mathbf{E}$ （矩阵可以分解为一系列初等矩阵的乘积）
4. $\exists \mathbf{B}_n,\ \mathbf{A}_n\mathbf{B}_n = \mathbf{I}_n$ （存在矩阵与之相乘等于单位阵）
5. $\lambda_i(\mathbf{A}_n) \ne 0$ （矩阵的特征值都不等于零）
6. $\mathbf{A}_n\mathbf{x} = \mathbf{0}$ 只有零解
7. $\mathbf{A}_n\mathbf{x} = \mathbf{b}$ 有唯一解
8. 矩阵的行向量线性无关
9. 矩阵的列向量线性无关
10. 矩阵的特征向量线性无关
11. 矩阵可以进行 LU 分解
12. 矩阵可以进行 QR 分解

 ……

在诸多等价的命题中，我们最关心的是第一个。

# 矩阵行列式的求解

矩阵求逆有很多种方法，其中最常用的方法是伴随矩阵法和 Gauss-Jordan 消元法。我们选择对第一种方法进行编程实现。这就绕不开矩阵行列式的求解。

矩阵行列式的定义如下。

$$
|\mathbf{A}_n|=\sum_{i=1}^{n}a_{ij}C_{ij}=\sum_{j=1}^{n}a_{ij}C_{ij}
$$

其中， $C_{ij}$ 为代数余子式，它与余子式 $M_{ij}$ 的关系如下。

$$
C_{ij} = (-1)^{i+j}M_{ij}
$$

我们知道， $a_{ij}$ 所对应的余子式 $M_{ij}$ 是原矩阵 $\mathbf{A}_n$ 在划去第 $i$ 行和第 $j$ 列后剩余部分的行列式。即

$$
M_{ij}=|\mathbf{A}_{n-1}(i,\ j)|
$$

所以，行列式存在递归定义。

## 递归法求行列式

递归的本质其实就是数学归纳法。

当 $n=1$ 时，行列式的值就直接等于这个数。

当 $n>1$ 时，有

$$
|\mathbf{A}_n|=\sum_{i=1}^{n}(-1)^{i+j}a_{ij}|\mathbf{A}_{n-1}(i,\ j)|
$$

递归法求行列式用代码描述如下。

```c++
// 求余子阵
Matrix Matrix::Submatrix(int x, int y) {
  Matrix S(this->n - 1);  // 构造 n - 1 维矩阵
  for (int i = 0; i < S.n; ++i)
    for (int j = 0; j < S.n; ++j)
      S.at(i, j) = this->at((i < x) ? i : (i + 1), (j < y) ? j : (j + 1));
  return S;
}
// 递归法求行列式
double Matrix::Determinant() {
  double result = 0.0;
  // 沿第一行展开
  for (int j = 0; j < this->n; ++j) 
    result += ((j % 2 == 0) ? 1 : -1) * this->at(0, j) 
        * Determinant(Submatrix(0, j));
  return result;
}
// 递归法求行列式（重载）
double Matrix::Determinant(Matrix S) {
  if (S.n == 1)
    return S.at(0, 0);
  else
    return S.Determinant();
}
```

## 伪 LU 分解法求行列式

当计算量较大时，递归将会造成大量的开销。我们期望找到一种顺序算法来取代递归算法。

接下来介绍一种通过对矩阵进行伪 LU 分解来求取矩阵行列式的算法。

矩阵 LU 分解的模型为

$$
\mathbf{A}_n = \mathbf{L}_n\mathbf{U}_n
$$

即

$$
\left\lbrace
\begin{aligned}
  a_{ij} &= \sum_{k=1}^nl_{ik}u_{kj} \\
  l_{ik} &= 0,\quad i<k \\
  u_{kj} &= 0,\quad j<k
\end{aligned}
\right.
$$

在进行分类讨论后，该模型可简化为

$$
a_{ij}=\sum_{k=1}^{\min(i,j)}l_{ik}u_{kj}
$$

![矩阵LU分解分类讨论](Fig2.1.png)

现已知 $a_{ij}$ 值，反解 $l_{ij}$ 和 $u_{ij}$ 。不妨令 $m=\min(i,\ j)$ ，有

$$
l_{im}u_{mj}=a_{ij}-\displaystyle\sum_{k=1}^{m-1}l_{ik}u_{kj}
$$

为了确定一组解，可以取 $\mathbf{L}_n$ 阵对角线元素为 $1$ 。

将 $\mathbf{L}_n$ 阵除主对角线外的其余非零元素与 $\mathbf{U}_n$ 阵的非零元素拼接成 $\mathbf{B}_n$ 阵，即

$$
b_{ij} =
\left\lbrace
\begin{aligned}
  u_{ij}, &\quad i \le j \\
  l_{ij}, &\quad i \gt j
\end{aligned}
\right.
$$

取 $\mathbf{B}_n$ 初始值为单位阵 $\mathbf{I}_n$ ，则可按下式顺序计算 $\mathbf{B}_n$ 阵各元素值。

$$
b_{ij}= \dfrac{1}{b_{jj}}\Big(a_{ij}-\displaystyle\sum_{k=1}^{m-1}l_{ik}u_{kj}\Big)
$$

从而计算出 $\mathbf{L}_n$ 阵与 $\mathbf{U}_n$ 阵的各非零元素。

一旦矩阵可以进行 LU 分解，行列式的求取将变得异常简单。

$$
|\mathbf{A}_n|=|\mathbf{L}_n|\times|\mathbf{U}_n|=1\times\prod_{j=1}^{n}u_{jj}=\prod_{j=1}^{n}b_{jj}
$$

不过，矩阵可以进行 LU 分解是矩阵可逆的充要条件之一。在此之前，我们是不知道矩阵是否可逆的。

但是，这并不影响我们求取行列式。因为在上述计算过程中， $b_{jj}$ 的值将被覆写。如果 $b_{jj}=0$ ，则 $|\mathbf{A}_n|=0$ ，矩阵不可逆。同时说明 $\mathbf{U}_n$ 阵主对角线元素存在零，不是上三角矩阵——即进行了“伪 LU 分解”。

伪 LU 分解法求行列式用代码描述如下。

```c++
// 伪 LU 分解法求行列式
double Matrix::Determinant() {
  double result = 1.0;
  Matrix B(n);
  // 将矩阵 B 初始化为单位阵
  MakeItIdentity(B);
  for (int i = 0; i < B.n; ++i) {
    for (int j = 0; j < B.n; ++j) {
      if (fabs(B.at(j, j)) < 1e-15)
        return 0.0;
      double temp = 0.0;
      for (int k = 0; k < min(i, j); ++k)
        temp += B.at(i, k) * B.at(k, j);
      B.at(i, j) = (this->at(i, j) - temp) / B.at(j, j);
      if (i == j)
        result *= B.at(i, j);
    }
  }
  return result;
}
```

# 伴随矩阵法求逆

知道了矩阵行列式的求法，那么伴随矩阵的问题将迎刃而解。下面直接给出伴随矩阵法求逆的代码实现。

```c++
// 求伴随矩阵
Matrix Matrix::AdjointMatrix() {
  Matrix A(n);
  for (int i = 0; i < n; ++i)
    for (int j = 0; j < n; ++j)
      A.at(j, i) = 
        (((i + j) % 2 == 0) ? 1 : -1) * Submatrix(i, j).Determinent();
  return A;
}
// 伴随矩阵法求逆
Matrix Matrix::Inverse() {
  return AdjointMatrix() * (1.0 / Determinant());
}
```

[目录](./../../README.md)
