测量平差有两个主要的任务：参数估计和精度评定。
其数学模型也包括两个方面：函数模型和随机模型。

# 函数模型

测量平差用到的函数模型如下。

$$
\mathbf{F}(\mathbf{L},\ \mathbf{X})=\mathbf{0}
$$

其中， $\mathbf{L}$ 为观测值， $\mathbf{X}$ 为参数值。
我们在近似值 $(\mathbf{(L)},\ \mathbf{(X)})$ 处对 $\mathbf{F}$ 作泰勒展开，

$$
\mathbf{F}(\mathbf{L},\ \mathbf{X})=\mathbf{F}(\mathbf{(L)},\ \mathbf{(X)})+\frac{\partial{\mathbf{F}}}{\partial{\mathbf{L}}}(\mathbf{(L)})(\mathbf{L}-\mathbf{(L)})+\frac{\partial{\mathbf{F}}}{\partial{\mathbf{X}}}(\mathbf{(X)})(\mathbf{X}-\mathbf{(X)})+\mathbf{R}_n(\mathbf{L},\ \mathbf{X})
$$

忽略高阶项 $\mathbf{R}_n$ 后，带入函数模型得到：

$$
\frac{\partial{\mathbf{F}}}{\partial{\mathbf{L}}}(\mathbf{(L)})(\mathbf{L}-\mathbf{(L)})+\frac{\partial{\mathbf{F}}}{\partial{\mathbf{X}}}(\mathbf{(X)})(\mathbf{X}-\mathbf{(X)})+\mathbf{F}(\mathbf{(L)},\ \mathbf{(X)})=\mathbf{0}
$$

这就是我们要求解的方程组，为简化表述，我们将上面的方程写做：

$$
\mathbf{J_{F_L}}\mathbf{v}+\mathbf{J_{F_X}}\mathbf{x}+\mathbf{C}=\mathbf{0}
$$

其中， $\mathbf{v}$ 代表观测值改正数， $\mathbf{x}$ 代表参数值改正数。
雅可比矩阵 $\mathbf{J}$ 和常数阵 $\mathbf{C}$ 通过下列公式计算：

$$
\left\lbrace
\begin{aligned}
  \mathbf{J_{F_L}} &= \dfrac{\partial{\mathbf{F}}}{\partial{\mathbf{L}}}(\mathbf{(L)}) \\
  \mathbf{J_{F_X}} &= \dfrac{\partial{\mathbf{F}}}{\partial{\mathbf{X}}}(\mathbf{(X)}) \\
  \mathbf{C} &= \mathbf{F}(\mathbf{(L)},\ \mathbf{(X)})
\end{aligned}
\right.
$$

## 最小二乘准则

在函数模型中，未知数的个数往往多于方程的个数。这意味着，我们在解一个超定方程组，这是一个优化问题。

比如，在第一章第二节一维坐标情形的例子中，未知数的个数有四个，而方程的个数只有一个，即：

$$
\begin{aligned}
  \mathbf{L} &= \begin{bmatrix} h_{A1} & h_{12} & h_{23} & h_{3B} \end{bmatrix}^T \\
  \mathbf{F}(\mathbf{L},\ \mathbf{0}) &= \begin{bmatrix} H_A+h_{A1}+h_{12}+h_{23}+h_{3B}-H_B \end{bmatrix}
\end{aligned}
$$

当时，我们人为地创造了条件，认为各项误差相等：

$$
v_{A1} = v_{12} = v_{23} = v_{3B}
$$

这相当于补充了三个方程，从而解出了改正数 $\mathbf{v}$ 。

但是，在各种因素的影响下，各项误差并不总是相等。况且各项误差相等的这一组解并不一定是问题的最优解。

在超定方程组的诸多解中，我们期望找到这样一组解，它能使各项误差的加权平方和最小——这就是大名鼎鼎的最小二乘准则。

$$
\mathbf{v}^T\mathbf{P}\mathbf{v}=\min
$$

式中的 $\mathbf{P}$ 代表权阵。

## 函数模型的解

现在，我们可以构造拉格朗日函数：

$$
\mathbf{\Phi}(\mathbf{v},\ \mathbf{x})=\mathbf{v}^T\mathbf{P}\mathbf{v}+\mathbf{\lambda}(\mathbf{J_{F_L}}\mathbf{v}+\mathbf{J_{F_X}}\mathbf{x}+\mathbf{C})
$$

其中， $\mathbf{\lambda}$ 为拉格朗日乘子。为了简化公式推导，我们通常设 $\mathbf{\lambda}=-2\mathbf{K}^T$ 。

令

$$
\left\lbrace
\begin{aligned}
  \frac{\partial{\mathbf{\Phi}}}{\partial{\mathbf{v}}} &= \mathbf{0} \\
  \frac{\partial{\mathbf{\Phi}}}{\partial{\mathbf{x}}} &= \mathbf{0}
\end{aligned}
\right.
$$

有

$$
\left\lbrace
\begin{aligned}
  2\mathbf{v}^T\mathbf{P}-2\mathbf{K}^T\mathbf{J_{F_L}}=\mathbf{0} \\
  -2\mathbf{K}^T\mathbf{J_{F_X}}=\mathbf{0}
\end{aligned}
\right.
$$

第一式化简后得 $\mathbf{v}=\mathbf{P}^{-T}\mathbf{J_{F_L}}^T\mathbf{K}$ ，回代到函数模型中有

$$
\mathbf{J_{F_L}}\mathbf{P}^{-T}\mathbf{J_{F_L}}^T\mathbf{K}+\mathbf{J_{F_X}}\mathbf{x}+\mathbf{C}=\mathbf{0}
$$

解得 $\mathbf{K}=-(\mathbf{J_{F_L}}\mathbf{P}^{-T}\mathbf{J_{F_L}}^T)^{-1}(\mathbf{J_{F_X}}\mathbf{x}+\mathbf{C})$ ，代入第二式有

$$
\mathbf{J_{F_X}}^T(\mathbf{J_{F_L}}\mathbf{P}^{-T}\mathbf{J_{F_L}}^T)^{-1}(\mathbf{J_{F_X}}\mathbf{x}+\mathbf{C})=\mathbf{0}
$$

解得 $\mathbf{x}=-\left(\mathbf{J_{F_X}}^T(\mathbf{J_{F_L}}\mathbf{P}^{-T}\mathbf{J_{F_L}}^T)^{-1}\mathbf{J_{F_X}}\right)^{-1}\mathbf{J_{F_X}}^T(\mathbf{J_{F_L}}\mathbf{P}^{-T}\mathbf{J_{F_L}}^T)^{-1}\mathbf{C}$ 。

因此，在最小二乘准则的约束条件下，我们求得超定方程组的解为：

$$
\left\lbrace
\begin{aligned}
  \mathbf{x} &= -\left(\mathbf{J_{F_X}}^T(\mathbf{J_{F_L}}\mathbf{P}^{-T}\mathbf{J_{F_L}}^T)^{-1}\mathbf{J_{F_X}}\right)^{-1}\mathbf{J_{F_X}}^T(\mathbf{J_{F_L}}\mathbf{P}^{-T}\mathbf{J_{F_L}}^T)^{-1}\mathbf{C} \\
  \mathbf{v} &= -\mathbf{P}^{-T}\mathbf{J_{F_L}}^T(\mathbf{J_{F_L}}\mathbf{P}^{-T}\mathbf{J_{F_L}}^T)^{-1}(\mathbf{J_{F_X}}\mathbf{x}+\mathbf{C})
\end{aligned}
\right.
$$

特别的，如果 $\mathbf{J_{F_L}}$ 阵可逆，上述解的形式将得到大大简化：

$$
\left\lbrace
\begin{aligned}
  \mathbf{x} &= -\left((\mathbf{J_{F_L}}^{-1}\mathbf{J_{F_X}})^T\mathbf{P}^T(\mathbf{J_{F_L}}^{-1}\mathbf{J_{F_X}})\right)^{-1}(\mathbf{J_{F_L}}^{-1}\mathbf{J_{F_X}})^T\mathbf{P}^T(\mathbf{J_{F_L}}^{-1}\mathbf{C}) \\
  \mathbf{v} &= -\mathbf{J_{F_L}}^{-1}\mathbf{J_{F_X}}\mathbf{x}-\mathbf{J_{F_L}}^{-1}\mathbf{C}
\end{aligned}
\right.
$$

那什么时候 $\mathbf{J_{F_{L}}}$ 阵可逆呢？

---

> 证明： 在测量平差的函数模型中， $\mathbf{J_{F_L}}$ 可逆的充要条件是存在显式函数关系 $\mathbf{L}=\mathbf{L}(\mathbf{X})$ 。

1. 充分性证明

当 $\mathbf{J_{F_L}}$ 可逆时，函数模型可化为

$$
\begin{aligned}
  \mathbf{v} &= -\mathbf{J_{F_L}}^{-1}\mathbf{J_{F_X}}\mathbf{x}-\mathbf{J_{F_L}}^{-1}\mathbf{C} \\
  \mathbf{L}-\mathbf{(L)} &= -\mathbf{J_{F_L}}^{-1}\mathbf{J_{F_X}}(\mathbf{X}-\mathbf{(X)})-\mathbf{J_{F_L}}^{-1}\mathbf{C} \\
  \mathbf{L} &= -\mathbf{J_{F_L}}^{-1}\mathbf{J_{F_X}}(\mathbf{X}-\mathbf{(X)})-\mathbf{J_{F_L}}^{-1}\mathbf{C}+\mathbf{(L)}
\end{aligned}
$$

充分性得证。

2. 必要性证明

当存在 $\mathbf{L}=\mathbf{L}(\mathbf{X})$ 时，设 $\mathbf{Y}(\mathbf{L},\ \mathbf{X})=\mathbf{L}-\mathbf{L}(\mathbf{X})$ ，将其在 $(\mathbf{(L)},\ \mathbf{(X)})$ 处泰勒展开，忽略高阶项有，

$$
\begin{aligned}
  \frac{\partial{\mathbf{Y}}}{\partial{\mathbf{L}}}(\mathbf{(L)})(\mathbf{L}-\mathbf{(L)})+\frac{\partial{\mathbf{Y}}}{\partial{\mathbf{X}}}(\mathbf{(X)})(\mathbf{X}-\mathbf{(X)})+\mathbf{Y}(\mathbf{(L)},\ \mathbf{(X)}) &= \mathbf{0} \\
  (\mathbf{L}-\mathbf{(L)})-\frac{\partial{\mathbf{L}}}{\partial{\mathbf{X}}}(\mathbf{(X)})(\mathbf{X}-\mathbf{(X)})+\mathbf{(L)}-\mathbf{L}(\mathbf{(X)}) &= \mathbf{0} \\
  \mathbf{v}-\mathbf{J_{L_X}}\mathbf{x}+\mathbf{y} &= \mathbf{0}
\end{aligned}
$$

得到 $\mathbf{v}=\mathbf{J_{L_X}}\mathbf{x}-\mathbf{y}$ ，其中，

$$
\left\lbrace
\begin{aligned}
  \mathbf{J_{L_X}} &= \frac{\partial{\mathbf{L}}}{\partial{\mathbf{X}}}(\mathbf{(X)}) \\
  \mathbf{y} &= \mathbf{(L)}-\mathbf{L}(\mathbf{(X)}) \end{aligned}
\right.
$$

另一方面，改正数的解为 $\mathbf{v}=-\mathbf{P}^{-T}\mathbf{J_{F_L}}^T(\mathbf{J_{F_L}}\mathbf{P}^{-T}\mathbf{J_{F_L}}^T)^{-1}(\mathbf{J_{F_X}}\mathbf{x}+\mathbf{C})$ 。

不妨设 $\mathbf{J_{L_X}}=-\mathbf{B}\mathbf{J_{F_X}}$ ，则

$$
\begin{aligned}
  \mathbf{J_{L_X}} &= -\mathbf{P}^{-T}\mathbf{J_{F_L}}^T(\mathbf{J_{F_L}}\mathbf{P}^{-T}\mathbf{J_{F_L}}^T)^{-1}\mathbf{J_{F_X}} \\
  -\mathbf{B}\mathbf{J_{F_X}} &= -\mathbf{P}^{-T}\mathbf{J_{F_L}}^T(\mathbf{J_{F_L}}\mathbf{P}^{-T}\mathbf{J_{F_L}}^T)^{-1}\mathbf{J_{F_X}} \\
  (\mathbf{B}-\mathbf{P}^{-T}\mathbf{J_{F_L}}^T(\mathbf{J_{F_L}}\mathbf{P}^{-T}\mathbf{J_{F_L}}^T)^{-1})\mathbf{J_{F_X}} &= \mathbf{0}
\end{aligned}
$$

因为 $\mathbf{J_{F_X}}\ne\mathbf{0}$ ，所以

$$
\begin{aligned}
  \mathbf{B}-\mathbf{P}^{-T}\mathbf{J_{F_L}}^T(\mathbf{J_{F_L}}\mathbf{P}^{-T}\mathbf{J_{F_L}}^T)^{-1} &= \mathbf{0} \\
  \mathbf{J_{F_L}}\mathbf{B} &= \mathbf{J_{F_L}}\mathbf{P}^{-T}\mathbf{J_{F_L}}^T(\mathbf{J_{F_L}}\mathbf{P}^{-T}\mathbf{J_{F_L}}^T)^{-1} \\
  \mathbf{J_{F_L}}\mathbf{B} &= \mathbf{I}
\end{aligned}
$$

即 $\mathbf{J_{F_L}}$ 可逆。

必要性得证。

---

在实际应用时，由于参数 $\mathbf{X}$ 是可以人为设定的，我们往往有意识地去构造显式关系 $\mathbf{L}=\mathbf{L}(\mathbf{X})$ ，进而计算 $\mathbf{J_{L_X}}$ 、 $\mathbf{y}$ ，然后作解为：

$$
\left\lbrace
\begin{aligned}
  \mathbf{x} &= \left(\mathbf{J_{L_X}}^T\mathbf{P}^T\mathbf{J_{L_X}}\right)^{-1}\mathbf{J_{L_X}}^T\mathbf{P}^T\mathbf{y} \\
  \mathbf{v} &= \mathbf{J_{L_X}}\mathbf{x}-\mathbf{y}
\end{aligned}
\right.
$$

# 随机模型

测量平差用到的随机模型如下。

$$
\mathbf{D}=\sigma_0^2\mathbf{Q}=\sigma_0^2\mathbf{P}^{-1}
$$

其中， $\mathbf{D}$ 为协方差阵， $\mathbf{Q}$ 为协因数阵， $\mathbf{P}$ 为权阵。

## 协方差传播定律

对于函数关系

$$
\mathbf{y}=\mathbf{y}(\mathbf{x})
$$

全微分得到

$$
\mathbf{dy}=\mathbf{J_{y_x}}\mathbf{dx}
$$

对等式两边取平方得到

$$
\begin{aligned}
  \mathbf{dy}^2 &= (\mathbf{J_{y_x}}\mathbf{dx})^2 \\
  \mathbf{dy}\mathbf{dy}^T &= \mathbf{J_{y_x}}\mathbf{dx}\mathbf{dx}^T\mathbf{J_{y_x}}^T \\
  \mathbf{D_{y}} &= \mathbf{J_{y_x}}\mathbf{D_{x}}\mathbf{J_{y_x}}^T
\end{aligned}
$$

其中， $\mathbf{D}$ 即协方差阵。上式即为协方差传播定律。

## 协因数传播定律

根据协方差传播定律，稍加推导，我们就得到了协因数传播定律。

$$
\mathbf{Q_{y}} = \mathbf{J_{y_x}}\mathbf{Q_{x}}\mathbf{J_{y_x}}^T
$$

## 权值的计算

当 $\mathbf{D}$ 不是对角阵时， $\mathbf{Q}$ 、 $\mathbf{P}$ 也不是对角阵。此时，权阵 $\mathbf{P}$ 主对角线上的元素将不再具有权的意义，而其在运算时依旧起到了权的作用。

当 $\mathbf{D}$ 是对角阵时， $\mathbf{Q}$ 、 $\mathbf{P}$ 也是对角阵。此时，权阵 $\mathbf{P}$ 主对角线上的元素才具有权的意义，即：

$$
p_{ii}=\frac{\sigma_{0}^2}{\sigma_{i}^2}
$$

如果一单位的方差是 $\sigma_0^2$ ，那么根据协方差传播定律， $n$ 单位的方差就是 $\sigma_{i}^2=n\sigma_{0}^2$ ，此时权值为 $p_{ii}=\dfrac{1}{n}$ 。

## 单位权方差

随机模型中的乘常数 $\sigma_0^2$ 称为单位权方差。为了定权，其值需要事先估计。

平差过后，我们需要通过统计检验的方法判断平差前后其值是否一致，以此来判断平差结果的正确性。

[目录](./../../README.md)
