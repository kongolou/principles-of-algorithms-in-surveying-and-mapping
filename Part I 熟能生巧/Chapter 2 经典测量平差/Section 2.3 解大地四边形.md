在大地四边形 ABCD 中，已知 A、B 两点，独立等精度测得图示各角，求 C、D 两点。
各已知值如下。

$$
\begin{matrix}
  x_A = 0 & (\beta_{1}) = 44\degree 59' 47'' & (\beta_{5}) = 45\degree 00' 15'' \\
  y_A = 0 & (\beta_{2}) = 45\degree 00' 10'' & (\beta_{6}) = 45\degree 00' 05'' \\
  x_B = 0 & (\beta_{3}) = 45\degree 00' 02'' & (\beta_{7}) = 44\degree 59' 50'' \\
  y_B = 100 & (\beta_{4}) = 44\degree 59' 55'' & (\beta_{8}) = 45\degree 00' 12''
\end{matrix}
$$

![大地四边形](Fig2.2.png)

# 两种思路

我们已经掌握了经典测量平差的基本原理，现在求解问题的关键在于函数关系的确定。
事实上，有两种确定函数关系的思路。

## 寻找隐式函数关系

根据经典测量平差函数模型，我们需要找出隐式函数关系

$$
\mathbf{F}(\mathbf{L},\ \mathbf{X})=\mathbf{0}
$$

我们可以很轻易地列出下面三个独立的方程。

$$
\left\lbrace
\begin{aligned}
  \sum_1^8 \beta_i &= 360\degree \\
  \beta_{1} + \beta_{8} &= \beta_{4} + \beta_{5} \\
  \beta_{2} + \beta_{3} &= \beta_{6} + \beta_{7} \\
\end{aligned}
\right.
$$

还有一个方程难以发现，由不同路径计算得到的边长要重合。

$$
\left\lbrace
\begin{aligned}
  S_{CD} &= S_{AB}\frac{\sin{\beta_{3}}}{\sin{\beta_{8}}}\frac{\sin{\beta_{1}}}{\sin{\beta_{6}}} \\
  S_{CD} &= S_{AB}\frac{\sin{\beta_{2}}}{\sin{\beta_{5}}}\frac{\sin{\beta_{4}}}{\sin{\beta_{7}}}
\end{aligned}
\right.
$$

自此，已然可以平差作解。但是，我们往往还会设定一些参数方便最终求值。

例如，设定 C、D 点的坐标值为参数 $\mathbf{X}$ ，那么根据正弦定理我们还可以列出下面四个的独立方程。

$$
\left\lbrace
\begin{aligned}
  \frac{S_{AB}}{\sin{\beta_{5}}} &= \frac{S_{BC}}{\sin{\beta_{2}}} \\
  \frac{S_{AB}}{\sin{\beta_{5}}} &= \frac{S_{AC}}{\sin{(\beta_{3}+\beta_{4})}} \\
  \frac{S_{AB}}{\sin{\beta_{8}}} &= \frac{S_{AD}}{\sin{\beta_{3}}} \\
  \frac{S_{AB}}{\sin{\beta_{8}}} &= \frac{S_{BD}}{\sin{(\beta_{1}+\beta_{2})}}
\end{aligned}
\right.
$$

终于，我们可以写出隐函数关系了。

1. 确定函数关系

$$
\mathbf{L}=
\begin{bmatrix}
  \beta_{1} \\
  \beta_{2} \\
  \beta_{3} \\
  \beta_{4} \\
  \beta_{5} \\
  \beta_{6} \\
  \beta_{7} \\
  \beta_{8}
\end{bmatrix},\qquad
\mathbf{X}=
\begin{bmatrix}
  x_{C} \\
  y_{C} \\
  x_{D} \\
  y_{D}
\end{bmatrix}
$$

$$
\mathbf{F}(\mathbf{L},\ \mathbf{X})=
\begin{bmatrix}
  \beta_{1} + \beta_{2} + \beta_{3} + \beta_{4} + \beta_{5} + \beta_{6} + \beta_{7} + \beta_{8} - 360\degree \\
  \beta_{1} - \beta_{4} - \beta_{5} + \beta_{8} \\
  \beta_{2} + \beta_{3} - \beta_{6} - \beta_{7} \\
  \sin{\beta_{1}}\sin{\beta_{3}}\sin{\beta_{5}}\sin{\beta_{7}} - \sin{\beta_{2}}\sin{\beta_{4}}\sin{\beta_{6}}\sin{\beta_{8}} \\
  S_{AB}^2\sin^2{\beta_{2}}-((x_{C}-x_{B})^2+(y_{C}-y_{B})^2)\sin^2{\beta_{5}} \\
  S_{AB}^2\sin^2{(\beta_{3}+\beta_{4})}-((x_{C}-x_{A})^2+(y_{C}-y_{A})^2)\sin^2{\beta_{5}} \\
  S_{AB}^2\sin^2{\beta_{3}}-((x_{D}-x_{A})^2+(y_{D}-y_{A})^2)\sin^2{\beta_{8}} \\
  S_{AB}^2\sin^2{(\beta_{1}+\beta_{2})}-((x_{D}-x_{B})^2+(y_{D}-y_{B})^2)\sin^2{\beta_{8}}
\end{bmatrix}
$$

2. 计算近似值

$$
\left\lbrace
\begin{aligned}
  (x_{C}) &= x_{B} + S_{AB}\frac{\sin{(\beta_{2})}}{\sin{(\beta_{5})}}\cos\Big((\alpha_{AB}+(\beta_{3})+(\beta_{4})-180\degree) \bmod 360\degree \Big) \\
  (y_{C}) &= y_{B} + S_{AB}\frac{\sin{(\beta_{2})}}{\sin{(\beta_{5})}}\sin\Big((\alpha_{AB}+(\beta_{3})+(\beta_{4})-180\degree) \bmod 360\degree \Big) \\
  (x_{D}) &= x_{A} + S_{AB}\frac{\sin{(\beta_{3})}}{\sin{(\beta_{8})}}\cos\Big((\alpha_{BA}-(\beta_{1})-(\beta_{2})-180\degree) \bmod 360\degree \Big) \\
  (y_{D}) &= y_{A} + S_{AB}\frac{\sin{(\beta_{3})}}{\sin{(\beta_{8})}}\sin\Big((\alpha_{BA}-(\beta_{1})-(\beta_{2})-180\degree) \bmod 360\degree \Big)
\end{aligned}
\right.
$$

$$
\mathbf{(L)}=
\begin{bmatrix}
  44\degree 59' 47'' \\
  45\degree 00' 10'' \\
  45\degree 00' 02'' \\
  44\degree 59' 55'' \\
  45\degree 00' 15'' \\
  45\degree 00' 05'' \\
  44\degree 59' 50'' \\
  45\degree 00' 12''
\end{bmatrix}=
\begin{bmatrix}
  0.785334 \\
  0.785446 \\
  0.785407 \\
  0.785373 \\
  0.785470 \\
  0.785422 \\
  0.785349 \\
  0.785456
\end{bmatrix},\qquad
\mathbf{(X)}=
\begin{bmatrix}
  99.997576 \\
  99.997955 \\
  99.995152 \\
  0.001454
\end{bmatrix}
$$

3. 计算雅可比矩阵和常数阵

$$
\left\lbrace
\begin{aligned}
  \mathbf{J_{F_L}} &= \dfrac{\partial{\mathbf{F}}}{\partial{\mathbf{L}}}(\mathbf{(L)}) \\
  \mathbf{J_{F_X}} &= \dfrac{\partial{\mathbf{F}}}{\partial{\mathbf{X}}}(\mathbf{(X)}) \\
  \mathbf{C} &= \mathbf{F}(\mathbf{(L)},\ \mathbf{(X)})
\end{aligned}
\right.
$$

4. 定权

独立等精度测角，即权 $\mathbf{P}=\mathbf{I_{8}}$ 。

5. 求解改正数

$$
\left\lbrace
\begin{aligned}
  \mathbf{x} &= -\left(\mathbf{J_{F_X}}^T(\mathbf{J_{F_L}}\mathbf{P}^{-T}\mathbf{J_{F_L}}^T)^{-1}\mathbf{J_{F_X}}\right)^{-1}\mathbf{J_{F_X}}^T(\mathbf{J_{F_L}}\mathbf{P}^{-T}\mathbf{J_{F_L}}^T)^{-1}\mathbf{C} \\
  \mathbf{v} &= -\mathbf{P}^{-T}\mathbf{J_{F_L}}^T(\mathbf{J_{F_L}}\mathbf{P}^{-T}\mathbf{J_{F_L}}^T)^{-1}(\mathbf{J_{F_X}}\mathbf{x}+\mathbf{C})
\end{aligned}
\right.
$$

$$
\mathbf{v}=
\begin{bmatrix}
  0.000052 \\
  -0.000024 \\
  -0.000024 \\
  0.000026 \\
  -0.000071 \\
  0.000005 \\
  0.000004 \\
  -0.000045
\end{bmatrix},\qquad
\mathbf{x}=
\begin{bmatrix}
  0.004721 \\
  -0.000354 \\
  0.002073 \\
  -0.001739
\end{bmatrix}
$$

6. 改正观测值及参数值

$$
\left\lbrace
\begin{aligned}
  \mathbf{L} &= \mathbf{(L)} + \mathbf{v} \\
  \mathbf{X} &= \mathbf{(X)} + \mathbf{x}
\end{aligned}
\right.
$$

$$
\mathbf{L}=
\begin{bmatrix}
  0.785386 \\
  0.785422 \\
  0.785383 \\
  0.785399 \\
  0.785399 \\
  0.785427 \\
  0.785353 \\
  0.785411
\end{bmatrix}=
\begin{bmatrix}
  44\degree 59' 58'' \\
  45\degree 00' 05'' \\
  44\degree 59' 57'' \\
  45\degree 00' 00'' \\
  45\degree 00' 00'' \\
  45\degree 00' 06'' \\
  44\degree 59' 51'' \\
  45\degree 00' 03'' \\
\end{bmatrix},\qquad
\mathbf{X}=
\begin{bmatrix}
  100.002297 \\
  99.997602 \\
  99.997225 \\
  -0.000285
\end{bmatrix}
$$

7. 评价平差结果的精度

题目中无其他信息，这里仅以计算单位权方差的后验值为例。

后验方差 $\sigma_0^2 = \dfrac{\mathbf{v}^T\mathbf{P}\mathbf{v}}{f} = 2.93\times 10^{-9}$ 。

## 构造显式函数关系

在上一节我们提到，如果可以通过设定参数值构造显式函数关系 $\mathbf{L}=\mathbf{L}(\mathbf{X})$ ，那么解的形式将得到大大简化。

实际上，我们刚才所设计的参数已经在无意中构造了显式函数关系：

$$
\left\lbrace
\begin{aligned}
  \beta_1 &= (\alpha_{AC}-\alpha_{AD}) \bmod 360\degree \\
  \beta_2 &= (\alpha_{AB}-\alpha_{AC}) \bmod 360\degree \\
  \beta_3 &= (\alpha_{BD}-\alpha_{BA}) \bmod 360\degree \\
  \beta_4 &= (\alpha_{BC}-\alpha_{BD}) \bmod 360\degree \\
  \beta_5 &= (\alpha_{CA}-\alpha_{CB}) \bmod 360\degree \\
  \beta_6 &= (\alpha_{CD}-\alpha_{CA}) \bmod 360\degree \\
  \beta_7 &= (\alpha_{DB}-\alpha_{DC}) \bmod 360\degree \\
  \beta_8 &= (\alpha_{DA}-\alpha_{DB}) \bmod 360\degree \\
\end{aligned}
\right.
$$

式中，各坐标方位角通过坐标反算得到。

1. 确定函数关系

$$
\mathbf{L}=\mathbf{L}(\mathbf{X})=
\begin{bmatrix}
  \alpha(x_A,\ y_A,\ x_C,\ y_C)-\alpha(x_A,\ y_A,\ x_D,\ y_D) \\
  \alpha(x_A,\ y_A,\ x_B,\ y_B)-\alpha(x_A,\ y_A,\ x_C,\ y_C) \\
  \alpha(x_B,\ y_B,\ x_D,\ y_D)-\alpha(x_B,\ y_B,\ x_A,\ y_A) \\
  \alpha(x_B,\ y_B,\ x_C,\ y_C)-\alpha(x_B,\ y_B,\ x_D,\ y_D) \\
  \alpha(x_C,\ y_C,\ x_A,\ y_A)-\alpha(x_C,\ y_C,\ x_B,\ y_B) \\
  \alpha(x_C,\ y_C,\ x_D,\ y_D)-\alpha(x_C,\ y_C,\ x_A,\ y_A) \\
  \alpha(x_D,\ y_D,\ x_B,\ y_B)-\alpha(x_D,\ y_D,\ x_C,\ y_C) \\
  \alpha(x_D,\ y_D,\ x_A,\ y_A)-\alpha(x_D,\ y_D,\ x_B,\ y_B)
\end{bmatrix} \bmod 360\degree
$$

2. 计算近似值

3. 计算雅可比矩阵和常数阵

$$
\left\lbrace
\begin{aligned}
  \mathbf{J_{L_X}} &= \frac{\partial{\mathbf{L}}}{\partial{\mathbf{X}}}(\mathbf{(X)}) \\
  \mathbf{y} &= \mathbf{(L)}-\mathbf{L}(\mathbf{(X)}) \end{aligned}
\right.
$$

4. 定权

5. 求解改正数

$$
\left\lbrace
\begin{aligned}
  \mathbf{x} &= \left(\mathbf{J_{L_X}}^T\mathbf{P}^T\mathbf{J_{L_X}}\right)^{-1}\mathbf{J_{L_X}}^T\mathbf{P}^T\mathbf{y} \\
  \mathbf{v} &= \mathbf{J_{L_X}}\mathbf{x}-\mathbf{y}
\end{aligned}
\right.
$$

6. 改正观测值及参数值

7. 评价平差结果的精度

[目录](./../../README.md)
