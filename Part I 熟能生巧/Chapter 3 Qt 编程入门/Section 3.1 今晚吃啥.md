这一节，我们将用 Qt 制作一个桌面应用程序。它将完成以下几项简单的功能：

1. 读取菜单文件
2. 随机生成菜名
3. 记录美好生活
4. 感悟至理名言

程序的流程图如下。

![“今晚吃啥”程序流程图](Fig3.1.png)

我们的设想是，在一开始进入程序时，用户只能通过导入菜单来介入流程。在导出日志之后，程序不会自动退出，用户依然可以进行导入菜单和随机选菜的逻辑交互。

# 程序设计

打开 Qt，按照默认配置新建项目 `Dinner` ，项目将包含如下文件。

```plaintext
CMakeLists.txt
CMakeLists.txt.user
main.cpp
mainwindow.cpp
mainwindow.h
mainwindow.ui
```

打开其中的 `mainwindow.ui` 文件，届时 Qt 将跳转到“设计”界面，接下来由我们拖拽控件并设置属性完成图示窗口。

![“今晚吃啥”程序界面设计](Fig3.2.png)

注意控件的对象名称 `objectName` 和其显示文本 `title` 或者 `text` 不是同一个属性。另外，有关随机选菜和导出日志功能方面的控件，需要将其 `enabled` 属性的默认值设置为 `false` 。

## 信号与槽

在"设计"界面的下方，找到 `Signals and Slots Editor` 并新建条目如上图。保存修改后，单击 Qt 左下角的播放按钮。这时将弹出我们刚才设计好的窗口。

在菜单栏中选择：文件 -> 退出程序，程序将退出——注意我们刚才没有碰一行代码，就已经能够实现一个简单的逻辑交互功能了。

## 自定义槽

然而在大多数情况下，内置的槽函数不足以满足我们个性化的需求，我们就需要自己编写槽函数来响应控件发出的信号。

回到“编辑”界面，编辑头文件 `mainwindow.h` ，在 `MainWindow` 类中补充如下定义。

```c++
private slots:
  void read_menu();  // 文件 -> 读取菜单
  void write_log();  // 文件 -> 写进日志
  void get_lucky();  // 工具 -> 今晚吃啥
  void about_me();   // 帮助 -> 关于
```

上面的四个槽函数分别对应了菜单栏中其他四个选项的交互逻辑。
编辑源文件 `mainwindow.cpp` ，我们照葫芦画瓢，在构造函数中追加如下内容。

```c++
connect(ui->action_read_menu, SIGNAL(triggered(bool)), this, SLOT(read_menu()));
connect(ui->action_write_log, SIGNAL(triggered(bool)), this, SLOT(write_log()));
connect(ui->action_get_lucky, SIGNAL(triggered(bool)), this, SLOT(get_lucky()));
connect(ui->action_about_me, SIGNAL(triggered(bool)), this, SLOT(about_me()));
// 将主界面上“今晚吃啥”按钮的交互逻辑映射到菜单栏“工具”选项卡的“今晚吃啥”选项上
connect(ui->pushButton_choose, SIGNAL(clicked(bool)), this, SLOT(get_lucky()));
```

## 访问文件

继续编辑源文件 `mainwindow.cpp` ，我们来实现程序的基本功能之一——读取文件。

提到读取文件，也许你会首先想到 `<fstream>` ，但是 Qt 中存在更好的替代 `<QFile>` 。

```c++
// TODO: #include <QFile>
QFile menu("menu_path");
if (!menu.open(QIODevice::ReadOnly | QIODevice::Text))
  return;
```

上面的代码等价于

```c++
// TODO: #include <fstream>
std::fstream fin;
fin.open("filename", std::ios::in);
if(!fin.is_open())
  return;
```

1. 获取文件路径

回忆一下我们在使用其他软件时的情景，我们是通过一个文件会话窗口来获取文件的。在 Qt 中，我们可以通过 `<QFileDialog>` 实现这个文件会话窗口。

```c++
// TODO: #include <QDir>
// TODO: #include <QFileDialog>
QString here = QDir::currentPath();  // 获取当前目录作为默认目录
QString menu_path = QFileDialog::getOpenFileName(this, "Select menu",
                                                 here, "Text Files (*.txt)");
if (menu_path.isEmpty())
  return;
```

2. 存储所读数据

接下来，我们需要用一个变量来存储读入的数据。比如，对于下面这种模样的菜单文件，我们可以考虑使用一个数组来存储各条目。

```plaintext
红烧茄子
肉沫豆角
宫保鸡丁
鱼香肉丝
清蒸排骨
香菇炖肉
```

编辑头文件 `mainwindow.h` ，在 `MainWindow` 类中加入一个静态成员。

```c++
public:
  static QList<QString> dishes;
```

这里当然可以使用 `<vector>` 来替代 `<QList>` ，不过我们入乡随俗。

编辑源文件 `mainwindow.cpp` ，在 `MainWindow` 类外初始化该静态成员。

```c++
QList<QString> MainWindow::dishes;
```

这样我们就可以利用 `dishes` 来存储读取的数据了。

```c++
QTextStream in(&menu);
while (!in.atEnd()) {
  QString line = in.readLine();
  MainWindow::dishes.push_back(line);
}
```

出于同样的理由，这里使用 `<QTextStream>` 替代 `<iostream>` 来实现输入输出流。值得注意的是， `<QTextStream>` 默认文件采用 `UTF-8` 编码，如果文件采用其他格式编码，则需要先调用 `QTextStream::setEncoding()` 函数指定。

根据程序流程图，在获得菜单数据之后，我们就可以开启抽签和写日志的功能了。

```c++
ui->action_get_lucky->setEnabled(true);   // 打开“今晚吃啥”选项
ui->pushButton_choose->setEnabled(true);  // 打开“今晚吃啥”按钮
ui->action_write_log->setEnabled(true);   // 打开“写进日志”选项
```

自此，我们完成了槽函数 `read_menu()` 的全部基本逻辑，其完整代码详见附录。

槽函数 `write_log()` 的实现与 `read_menu()` 高度相似，此处不再赘述。

## 生成随机数

在用户提出“今晚吃啥”后，我们需要根据菜单随机抽取一道菜加以显示。

问题的关键在于生成随机数，好在 Qt 已经在 `<QRandomGenerator>` 中帮我们造好了轮子。所以代码实现就变得非常简单了。

```c++
// TODO: #include <QRandomGenerator>
if (dishes.isEmpty())
  return;
auto rnd = QRandomGenerator::global()->generate();  // 生成随机数
auto idx = rnd % dishes.size();                     // 计算下标
ui->label_dinner->setText(dishes[idx]);             // 展示选中的菜
```

将鼠标悬停在 `auto` 关键字上，经解析得知 `rnd` 将被推导为无符号整型数，所以我们可以在这里通过取余运算符 `%` 来实现取模运算的功能。

# 软件维护

旅程尚未结束——作为一名合格的独立开发者，我们需要对自己的软件负责。

软件的维护主要分为两大方面。

1. 完善程序现有的功能
2. 应用户需求增加新的功能

## 完善现有功能

软件维护的过程离不开一遍遍的代码测试。
你能发现程序中的不足吗？

1. 温馨提示

在刚才的的程序中，当打开文件失败时，我们只是简单地返回了，这样在程序窗口将看不出任何变化。

软件是为人服务的，需要时刻以人为本。在刚才的情况下，用户理应收到一个友好的提示，来被告知文件打开失败了。对此，我们可以用 `<QMessageBox>` 来实现一个简单的消息弹出窗口。

```c++
// TODO: #include <QMessageBox>
QMessageBox msg;
msg.setText("打开文件失败");
msg.exec();
```

在所有该产生反馈的地方，都应进行合理的消息提示。

2. 查漏补缺

我们尚未实现格言功能，因而忽视了程序主界面中的格言部分。

在程序主界面中， `lineEdit_sayings` 控件是可编辑的——这留下了一个潜在的安全隐患，因为用户可以借此产生某些未定义的行为。

解决方法之一是将 `lineEdit_sayings` 控件的 `enable` 属性设置为 `false` 。

3. 代码如诗

可读性是评价代码的重要标准之一。当我们不知代码何为美观时候，不妨先来看看规范的代码长什么样。

下载 LLVM ，在 Qt 中，选择：工具 -> 美化器 -> ClangFormat -> 格式化当前文件。

## 新增其他功能

到目前为止，程序依然有改进的空间。比如，每日格言，制作菜单，一键记录等等。

附录给出了本程序的全部代码，读者还可以在此基础之上根据自己的想象力进一步探索。

[目录](./../../README.md)
