这一节我们来画一颗星星。

# 解五角星

在进行程序绘图之前，我们先来做点数学——在图像坐标系下，解五角星。

我们知道，正五角星的所有内角可解。

![解五角星](Fig3.3.png)

从图中不难看出，五角星的一个小内角是 $c$ ，一个大内角是 $7c$ 。其中 $c$ 满足下式。

$$
10c=360\degree
$$

这样一来，五角星的所有内角已知，而其各边长相等——这启发我们用测绘的知识来解算各点的坐标。

## 问题归化

首先将坐标系转化为熟悉的左手系。解五角星的问题就变成了支导线坐标正算问题。好消息是，我们不需要再进行平差处理。所以解算过程只需要用到第一章的知识。我们在第一章写的测绘通用计算函数库要派上用场了。

![五角星坐标正算](Fig3.4.png)

# 程序绘图

打开 Qt 新建项目 `Star` ，在项目资源文件夹下加入 `mycalc.h` 和 `mycalc.cpp` 。

```plaintext
CMakeLists.txt
CMakeLists.txt.user
main.cpp
mycalc.cpp
mycalc.h
widget.cpp
widget.h
widget.ui
```

同时在 `CMakeLists.txt` 中找到如下命令，并在其中添加 `mycalc.h` 和 `mycalc.cpp` 。

```
set(PROJECT_SOURCES
    main.cpp
    widget.cpp
    widget.h
    widget.ui
    mycalc.cpp
    mycalc.h
)
```

## 程序流程

让我们回忆一下第一章第二节中坐标正算的流程。

绘制五角星的程序流程为：计算转角弧度值 -> 计算坐标方位角 -> 坐标正算 -> 绘制图形并填充 -> 保存图片。

## 坐标正算

我们在测绘通用计算函数库中所编写的函数只是针对一次单独的计算。而现在我们需要进行一组计算，因此，有必要对相应函数进行扩展。

参考 `mycalc.h` 中的函数签名，我们在 `widget.h` 中补充如下声明。

```c++
public:
  QList<double> calc_betas(QList<int> degrees);
  QList<double> calc_alphas(double alpha0, QList<double> betas);
  QList<double> calc_xs(double x0, QList<double> dists, QList<double> alphas);
  QList<double> calc_ys(double y0, QList<double> dists, QList<double> alphas);
  QList<QPointF> get_star_points(double x0 = 0.0, double y0 = 0.0,
                                 double alpha0 = 0.0, double dist = 100.0);
```

在最后的 `get_star_points()` 函数中，我们利用前面的四个函数来完成坐标正算。

```c++
QList<QPointF> Widget::get_star_points(double x0, double y0, double alpha0,
                                       double dist) {
  QList<int> degrees{8, 7, 1, 7, 1, 7, 1, 7, 1};
  for (int i = 0; i < 9; ++i)
      degrees[i] *= 36;
  QList<double> betas = calc_betas(degrees);
  QList<double> alphas = calc_alphas(alpha0, betas);
  QList<double> dists(10, dist);
  // 为了使图像尽可能地居中，我们：
  // 1. 将起点边伸长为平均边长的 1.5 倍
  // 2. 将 y 轴平移 20 个单位
  dists[0] *= 1.5;
  QList<double> xs = calc_xs(x0, dists, alphas);
  QList<double> ys = calc_ys(y0 + 20.0, dists, alphas);
  QList<QPointF> points;
  auto x = xs.begin();
  auto y = ys.begin();
  for (; x != xs.end() && y != ys.end(); ++x, ++y)
    points.push_back(QPointF(*x, *y));
  return points;
}
```

## 绘制与保存

在窗口中放上一个标签——我们将利用 `<QLabel>` 中的 `pixmap` 属性展示图像，再放上绘制和保存的按钮。按照程序流程，保存按钮 `enabled` 属性的默认值应设置为 `false` 。

右击“绘制”按钮，选择“转到槽”，编写其槽函数内容如下。

```c++
// TODO: #include <QPainter>
QPixmap pix(300, 300);             // 这是一张画布，
pix.fill();                        // 洁白无暇。
QPainter pnt(&pix);                // 我把它交给画家。
QColor red{200, 0, 0};             // 画家准备了红色颜料，
pnt.setPen(red);                   // 他将笔蘸成红色，
pnt.setBrush(red);                 // 刷子蘸成红色。
QPolygonF plg{get_star_points()};  // 他构思着，
pnt.drawPolygon(plg);              // 一颗红星！
```

以上内容将完成绘制，我们将其放到标签中展示，然后打开“保存”按钮。

```c++
ui->label_star->setPixmap(pix);
ui->pushButton_save_star->setEnabled(true);
```

回到“设计”界面，右击“保存”按钮，选择“转到槽”，编写其槽函数内容如下。

```c++
QPixmap pix(ui->label_star->pixmap());
// 图像将被保存在程序可执行文件所在目录下
if(pix.save("star.png", "PNG")) {
  QMessageBox msg(this);
  msg.setText("保存成功!");
  msg.exec();
}
```

这样就完成了程序绘制与保存的全部逻辑功能。

# 消除魔数

在 `get_star_points()` 函数中，出现了很多“魔数”——这是一个不好的编程习惯。

下面我们来简单分析一下其中一些“魔数”的含义，并对程序中所出现的“魔数”加以消除。

## 角和边的数量

除了起始点和起始边坐标方位角，我们还知道九个转角和十条边长。这两个数分别对应了 `degrees` 的 `9` 和 `dists` 的 `10` 。问题在于，一共有十个点的坐标需要我们计算，这就意味着我们需要知道十个坐标方位角。即 `alphas` 所需要的维度是 `10` ，而 `betas` 只具有和 `degrees` 相同的维度 `9` 。

其实这中间相差的一个维度，就是起始边坐标方位角 `alpha0` 。这样一来， `alphas` 将和 `dists` 具有相同的维度 `10` 。因此，我们可以将“魔数” `9` 替换成 `degrees.size()` ，将“魔数” `10` 替换成 `alphas.size()` 。

## 图像尺寸

在 `get_star_points()` 函数中，还有一个与图像尺寸相关的“魔数” `dist` ，它是函数的默认形参。

我们所设计的标签 `label_star` 的尺寸为 `(300, 300)` ，而 `dist` 的值为 `100.0` 。我们还对图像位置做了调整，才让图像居中。

对于这类“魔数”，可以基于控件尺寸进行消除，比如

```c++
dist = ui->label_star->width() / 3.0;
```

实际上， `get_star_points()` 函数不需要设置这么多形参。在本书附录的代码中，对这个函数进行了重新设计。

[目录](./../../README.md)
