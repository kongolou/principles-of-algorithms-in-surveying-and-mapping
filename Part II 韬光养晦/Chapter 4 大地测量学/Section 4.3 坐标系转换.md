如何将一个大地空间直角坐标系 $O_1\text{-}X_1Y_1Z_1$ 中的点换算到另一个大地空间直角坐标系 $O_2\text{-}X_2Y_2Z_2$ 下呢？

# 布尔莎七参数转换模型

布尔莎七参数转换模型如下。

$$
\mathbf{L}_2=\lambda\mathbf{R}\mathbf{L}_1+\mathbf{\Delta }_O
$$

其中， $\lambda$ 为尺度参数， $\mathbf{\Delta}_O$ 为平移向量， $\mathbf{R}$ 为旋转矩阵。 $\mathbf{L}_1$ 和 $\mathbf{L}_2$ 为同一点在不同坐标系下对应的坐标向量。视 $\mathbf{L}_1$ 为已知值， $\mathbf{L}_2$ 为观测值。

## 旋转矩阵

旋转体绕旋转轴转过的角度 $\varepsilon$ 称为欧勒角，规定其正方向为右手螺旋方向。

![欧勒角](Fig4.1.png)

如图， $X_1OY_1$ 绕原点 $O$ 转过一欧勒角 $\varepsilon_Z$ 至 $X_2OY_2$ 。

![旋转变换](Fig4.2.png)

其中一点 $K$ 在两套坐标系下的直角坐标与极坐标对应关系如下。

$$
\left\lbrace
\begin{aligned}
x_{1} &= S\cos\alpha_1 \\
y_{1} &= S\sin\alpha_1 \\
x_{2} &= S\cos\alpha_2 \\
y_{2} &= S\sin\alpha_2
\end{aligned}
\right.
$$

利用 $\alpha_2=\alpha_1-\varepsilon_Z$ 作三角变换

$$
\left\lbrace
\begin{aligned}
\cos\alpha_2 &= \cos\alpha_1\cos\varepsilon_Z+\sin\alpha_1\sin\varepsilon_Z \\
\sin\alpha_2 &= \sin\alpha_1\cos\varepsilon_Z-\cos\alpha_1\sin\varepsilon_Z
\end{aligned}
\right.
$$

等式两边同时乘以极半径 $S$ ，得到

$$
\left\lbrace
\begin{aligned}
x_2 &= x_1\cos\varepsilon_Z+y_1\sin\varepsilon_Z \\
y_2 &= y_1\cos\varepsilon_Z-x_1\sin\varepsilon_Z
\end{aligned}
\right.
$$

即

$$
\begin{bmatrix}
x_2 \\
y_2
\end{bmatrix}
=
\begin{bmatrix}
\cos\varepsilon_Z & \sin\varepsilon_Z \\
-\sin\varepsilon_Z & \cos\varepsilon_Z
\end{bmatrix}
\begin{bmatrix}
x_1 \\
y_1
\end{bmatrix}
$$

将此结论推广至三维，得到

$$
\begin{bmatrix}
x_2 \\
y_2 \\
z_2
\end{bmatrix}
=
\begin{bmatrix}
1 & 0 & 0 \\
0 & \cos\varepsilon_X & \sin\varepsilon_X \\
0 & -\sin\varepsilon_X & \cos\varepsilon_X
\end{bmatrix}
\begin{bmatrix}
\cos\varepsilon_Y & 0 & -\sin\varepsilon_Y \\
0 & 1 & 0 \\
\sin\varepsilon_Y & 0 & \cos\varepsilon_Y
\end{bmatrix}
\begin{bmatrix}
\cos\varepsilon_Z & \sin\varepsilon_Z & 0 \\
-\sin\varepsilon_Z & \cos\varepsilon_Z & 0 \\
0 & 0 & 1
\end{bmatrix}
\begin{bmatrix}
x_1 \\
y_1 \\
z_1
\end{bmatrix}
$$

当各欧勒角足够小时，旋转矩阵简化为

$$
\mathbf{R}=
\begin{bmatrix}
1 & \varepsilon_Z & -\varepsilon_Y \\
-\varepsilon_Z & 1 & \varepsilon_X \\
\varepsilon_Y & -\varepsilon_X & 1
\end{bmatrix}
$$

## 求解七参数

布尔莎七参数为

$$
\mathbf{X}=\begin{bmatrix}
  \Delta{X}_O,\ \Delta{Y}_O,\ \Delta{Z}_O,\ \lambda,\ \varepsilon_{X},\ \varepsilon_{Y},\ \varepsilon_{Z}
\end{bmatrix}^T
$$

七参数中， $\lambda$ 的值接近于 1 ，欧勒角 $\varepsilon$ 的值接近于 0 。

根据布尔莎七参数转换模型，近似地有

$$
\begin{bmatrix}
  X_2 \\
  Y_2 \\
  Z_2
\end{bmatrix}=\begin{bmatrix}
  \lambda & \varepsilon_Z & -\varepsilon_Y \\
  -\varepsilon_Z & \lambda & \varepsilon_X \\
  \varepsilon_Y & -\varepsilon_X & \lambda
\end{bmatrix}\begin{bmatrix}
  X_1 \\
  Y_1 \\
  Z_1
\end{bmatrix}+\begin{bmatrix}
  \Delta{X}_O \\
  \Delta{Y}_O \\
  \Delta{Z}_O
\end{bmatrix}
$$

重组矩阵，得到显函数关系

$$
\begin{bmatrix}
  X_2 \\
  Y_2 \\
  Z_2
\end{bmatrix}=\begin{bmatrix}
  1 & 0 & 0 & X_1 & 0 & -Z_1 & Y_1 \\
  0 & 1 & 0 & Y_1 & Z_1 & 0 & -X_1 \\
  0 & 0 & 1 & Z_1 & -Y_1 & X_1 & 0
\end{bmatrix}\begin{bmatrix}
  \Delta{X}_O \\
  \Delta{Y}_O \\
  \Delta{Z}_O \\
  \lambda \\
  \varepsilon_{X} \\
  \varepsilon_{Y} \\
  \varepsilon_{Z}
\end{bmatrix}
$$

则

$$
\left\lbrace
\begin{aligned}
  \mathbf{J_{L_X}} &= \begin{bmatrix}
    1 & 0 & 0 & X_1 & 0 & -Z_1 & Y_1 \\
    0 & 1 & 0 & Y_1 & Z_1 & 0 & -X_1 \\
    0 & 0 & 1 & Z_1 & -Y_1 & X_1 & 0
  \end{bmatrix} \\
  \mathbf{y} &= (\mathbf{L}_2)-\mathbf{J_{L_X}}\times\mathbf{(X)}
\end{aligned}
\right.
$$

---
例题

已知 A、B、C 三点在两套坐标系下的坐标，求解七参数及 D 点在另一套坐标系下的坐标。

| 坐标/m  |            A |            B |            C |
| ----- | -----------: | -----------: | -----------: |
| $X_1$ | -2722720.343 | -2723130.106 | -2726120.504 |
| $Y_1$ |  4429306.301 |  4431260.780 |  4427716.358 |
| $Z_1$ |  3682136.625 |  3679550.261 |  3681514.511 |
| $X_2$ | -2722708.864 | -2723118.632 | -2726109.020 |
| $Y_2$ |  4429315.210 |  4431269.685 |  4427725.263 |
| $Z_2$ |  3682132.936 |  3679546.589 |  3681510.823 |

| 坐标/m  |            D |
| ----- | -----------: |
| $X_1$ | -2725958.007 |
| $Y_1$ |  4430477.961 |
| $Z_1$ |  3678382.473 |

---
答案

1. 七参数

|   $dX_O$ |   $dY_O$ | $dZ_O$ | $\lambda$ | $\varepsilon_X$ | $\varepsilon_Y$ | $\varepsilon_Z$ |
| -------: | -------: | -----: | --------: | --------------: | --------------: | --------------: |
| -13.2685 | -29.2514 | 7.8576 |  1.72E-06 |         0.4814″ |         0.0130″ |         0.3126″ |

2. D 点坐标

| 坐标/m  |            D |
| ----- | -----------: |
| $X_2$ | -2725946.528 |
| $Y_2$ |  4430486.870 |
| $Z_2$ |  3678378.793 |

[目录](./../../README.md)
