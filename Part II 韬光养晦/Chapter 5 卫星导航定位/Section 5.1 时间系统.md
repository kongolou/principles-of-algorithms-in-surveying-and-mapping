时间是一个一维坐标，其原点通常可以自由选取。根据其单位间隔定义的不同可以分为恒星时、平太阳时、原子时等时间系统。

<dl>
  <dt>恒星时（ Sidereal Time, ST ）</dt>
  <dd>以春分点为参照点的时间系统</dd>
  <dt>平太阳时（ Mean Solar Time, MT ）</dt>
  <dd>以平均速度运行的太阳为参照点的时间系统</dd>
  <dt>原子时（ Temps Atomique International, TAI ）</dt>
  <dd>以物质内部原子运动周期定义的时间系统</dd>
</dl>

由平太阳时和原子时还衍生出如下时间系统：

<dl>
  <dt>世界时（ Universal Time, UT ）</dt>
  <dd>格林尼治的平太阳时</dd>
  <dt>协调世界时（ Universal Time Coordinated, UTC ）</dt>
  <dd>将世界时的秒长部分改为原子时的秒长，并通过跳秒等方式来同步到世界时</dd>
  <dt>GPS 时（ GPS Time, GPST ）</dt>
  <dd>同样将世界时的秒长部分改为原子时的秒长，但不进行跳秒</dd>
</dl>

其他卫星时如北斗时（ BeiDou Time, BDS ）与 GPS 时类似，只是计时起点不同。

此外，还有一些其他的计时系统。

<dl>
  <dt>儒略日（ Julian Day, JD ）</dt>
  <dd>将年、月、日换算为一个连续增长的数字，常用于长时间计时。时间原点定义在公元前 4713 年 1 月 1 日的正午 12 点（ UTC ）</dd>
  <dt>简化儒略日（ Modified Julian Day, MJD ）</dt>
  <dd>将儒略日的时间原点平移到 1858 年 9 月 17 日的午夜零时。</dd>
  <dt>年积日（ Day of Year, DOY ）</dt>
  <dd>时间原点总是取在一年中的第一天，而时间间隔为一天的计时系统。</dd>
</dl>

[目录](./../../README.md)
