GNSS 单点定位分为伪码测距单点定位和载波相位测距单点定位等。
伪码测距定位又称为标准单点定位(Standard Point Positioning, SPP)。
载波相位测距定位又称为精密单点定位(Precious Point Positioning, PPP)。

设站星距为

$$
\rho_{RS}=\sqrt{(X_R-X_S)^2+(Y_R-Y_S)^2+(Z_R-Z_S)^2}
$$

其中， $R$ 代表地表的接收机， $S$ 代表天上的卫星。

# 伪码测距模型

伪码测距理论公式如下

$$
\rho_{RS}=c(t_R-t_S)
$$

实际观测值是时间，而坐标则是我们感兴趣的参数。即

$$
\mathbf{L}=\begin{bmatrix} t_R \end{bmatrix},\quad
\mathbf{X}=\begin{bmatrix} X_R \\ Y_R \\ Z_R \end{bmatrix}
$$

$$
\mathbf{F}(\mathbf{L},\ \mathbf{X})= \begin{bmatrix} \rho_{RS}-c(t_R-t_S) \end{bmatrix}
$$

$$
\left\lbrace
\begin{aligned}
  \mathbf{J_{F_L}} &= \begin{bmatrix} -c \end{bmatrix} \\
  \mathbf{J_{F_X}} &=
  \begin{bmatrix}
  \dfrac{(X_R)-X_S}{(\rho_{RS})} & \dfrac{(Y_R)-Y_S}{(\rho_{RS})} & \dfrac{(Z_R)-Z_S}{(\rho_{RS})}
  \end{bmatrix} \\
  \mathbf{C} &= \begin{bmatrix} (\rho_{RS})-c((t_R)-t_S) \end{bmatrix}
\end{aligned}
\right.
$$

# 载波相位测距模型

载波相位测距理论公式如下

$$
\rho_{RS}=\lambda(N+\varphi_S-\varphi_R)
$$

其中， $N$ 为整周模糊度，将成为我们待解的参数之一。即

$$
\mathbf{L}=\begin{bmatrix} \varphi_R \end{bmatrix},\quad
\mathbf{X}=\begin{bmatrix} X_R \\ Y_R \\ Z_R \\ N \end{bmatrix}
$$

$$
\mathbf{F}(\mathbf{L},\ \mathbf{X})=
\begin{bmatrix}
\rho_{RS}-\lambda(N+\varphi_S-\varphi_R)
\end{bmatrix}
$$

$$
\left\lbrace
\begin{aligned}
\mathbf{J_{F_L}} &= \begin{bmatrix} \lambda \end{bmatrix} \\
\mathbf{J_{F_X}} &=
\begin{bmatrix}
\dfrac{(X_R)-X_S}{(\rho_{RS})} & \dfrac{(Y_R)-Y_S}{(\rho_{RS})} & \dfrac{(Z_R)-Z_S}{(\rho_{RS})} & -\lambda
\end{bmatrix} \\
\mathbf{C} &= \begin{bmatrix} (\rho_{RS})-\lambda((N)+\varphi_S-(\varphi_R)) \end{bmatrix}
\end{aligned}
\right.
$$

[目录](./../../README.md)
