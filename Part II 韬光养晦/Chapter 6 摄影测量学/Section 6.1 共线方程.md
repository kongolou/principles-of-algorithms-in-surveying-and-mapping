共线方程是摄影测量学中最重要的内容之一。下面简述其推导过程。

如图，以摄影中心 S 为原点，建立像空间直角坐标系 S-xyz （右手系）。因为摄影中心 S 与像片中心 o 的距离为摄影机焦距 f ，所以，像片上任意点 (x, y) 在像空间直角坐标系中的坐标为 (x, y, -f) 。

![像空间坐标系](Fig6.1.png)

如图，以高斯平面上一点 P 为原点，建立地面摄影测量坐标系 P-UVW （右手系）。取辅助空间 S-uvw 为其平行子空间。

![辅助空间坐标系](Fig6.2.png)

像空间 S-xyz 与辅助空间 S-uvw 的关系为

$$
\begin{bmatrix}
	u \\
	v \\
	w
\end{bmatrix} = \mathbf{R}
\begin{bmatrix}
	x \\
	y \\
	-f
\end{bmatrix}
$$

其中， $\mathbf{R}$ 为旋转矩阵，具有正定性，即 $\mathbf{R}^{-1}=\mathbf{R}^T$ 。

对于辅助空间 S-uvw 中一点 K ，存在

$$
\overrightarrow{SK}=\overrightarrow{PK}-\overrightarrow{PS}
$$

即

$$
\begin{bmatrix}
	u \\
	v \\
	w
\end{bmatrix} =
\begin{bmatrix}
	U \\
	V \\
	W
\end{bmatrix} -
\begin{bmatrix}
	U_S \\
	V_S \\
	W_S
\end{bmatrix}
$$

从而推导出共线方程

$$
\begin{bmatrix}
	x \\
	y \\
	-f
\end{bmatrix} = \mathbf{R}^T
\begin{bmatrix}
	U-U_S \\
	V-V_S \\
	W-W_S
\end{bmatrix}
$$

式中， $x$ 、 $y$ 、 $f$ 合称为像片的内方位元素，而旋转矩阵中的三个角 $\varepsilon_x$ 、 $\varepsilon_y$ 、 $\varepsilon_z$ 与摄影中心在地面摄影测量坐标系中的坐标 $U_S$ 、 $V_S$ 、 $W_S$ 合称为像片的外方位元素。一旦像片的内、外方位元素确定，就可以解求地面上影像范围内任意点的坐标。

[目录](./../../README.md)
