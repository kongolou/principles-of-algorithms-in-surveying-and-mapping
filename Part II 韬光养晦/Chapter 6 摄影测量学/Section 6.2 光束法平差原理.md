在共线方程中，我们的观测值是内方位元素，而参数为外方位元素及地面点坐标。

设

$$
\mathbf{R}=
\begin{bmatrix}
  a_1 & a_2 & a_3 \\
  b_1 & b_2 & b_3 \\
  c_1 & c_2 & c_3
\end{bmatrix}
$$

则共线方程可表示为

$$
\begin{bmatrix} x \\ y \\ -f \end{bmatrix} =
\begin{bmatrix} a_1 \\ a_2 \\ a_3 \end{bmatrix} (U-U_S) +
\begin{bmatrix} b_1 \\ b_2 \\ b_3 \end{bmatrix} (V-V_S) +
\begin{bmatrix} c_1 \\ c_2 \\ c_3 \end{bmatrix} (W-W_S)
$$

即

$$
\left\lbrace
\begin{aligned}
  x &= -f\frac{a_1(U-U_S)+b_1(V-V_S)+c_1(W-W_S)}{a_3(U-U_S)+b_3(V-V_S)+c_3(W-W_S)} \\
  y &= -f\frac{a_2(U-U_S)+b_2(V-V_S)+c_2(W-W_S)}{a_3(U-U_S)+b_3(V-V_S)+c_3(W-W_S)} \\
\end{aligned}
\right.
$$

我们能够将观测值表示为参数值的显式函数，这意味着参数值解的形式为

$$
\mathbf{x}=\left(\mathbf{J_{L_X}}^T\mathbf{P}^T\mathbf{J_{L_X}}\right)^{-1}\mathbf{J_{L_X}}^T\mathbf{P}^T\mathbf{y}
$$

对于一张像片而言，其外方位元素参数与坐标参数不同质，需要分开解算。

在解算时，我们认为 $\mathbf{P}=\mathbf{I}$ 。

首先将参数值的解化为法方程的形式。

$$
\mathbf{J_{L_X}}^T\mathbf{J_{L_X}}\mathbf{x}=\mathbf{J_{L_X}}^T\mathbf{y}
$$

然后对参数值进行分块

$$
\mathbf{X}=\begin{bmatrix} \mathbf{X}_1 \\ \mathbf{X}_2 \end{bmatrix}, \quad
\mathbf{X}_1=
\begin{bmatrix}
  U_S \\
  V_S \\
  W_S \\
  \varepsilon_x \\
  \varepsilon_y \\
  \varepsilon_z
\end{bmatrix}, \quad
\mathbf{X}_2=
\begin{bmatrix}
  U \\
  V \\
  W
\end{bmatrix}
$$

则法方程变为

$$
\begin{aligned}
  \begin{bmatrix} \mathbf{J}_1 & \mathbf{J}_2 \end{bmatrix}^T
  \begin{bmatrix} \mathbf{J}_1 & \mathbf{J}_2 \end{bmatrix}
  \begin{bmatrix} \mathbf{x}_1 \\ \mathbf{x}_2 \end{bmatrix} &=
  \begin{bmatrix} \mathbf{J}_1 & \mathbf{J}_2 \end{bmatrix}^T \mathbf{y} \\
  \begin{bmatrix} \mathbf{J}_1^T \\ \mathbf{J}_2^T \end{bmatrix}
  \begin{bmatrix} \mathbf{J}_1 & \mathbf{J}_2 \end{bmatrix}
  \begin{bmatrix} \mathbf{x}_1 \\ \mathbf{x}_2 \end{bmatrix} &=
  \begin{bmatrix} \mathbf{J}_1^T \\ \mathbf{J}_2^T \end{bmatrix} \mathbf{y}
\end{aligned}
$$

将其化成方程组的形式

$$
\left\lbrace
\begin{aligned}
  \mathbf{J}_1^T\mathbf{J}_1\mathbf{x}_1 + \mathbf{J}_1^T\mathbf{J}_2\mathbf{x}_2 &= \mathbf{J}_1^T\mathbf{y} \\
  \mathbf{J}_2^T\mathbf{J}_1\mathbf{x}_1 + \mathbf{J}_2^T\mathbf{J}_2\mathbf{x}_2 &= \mathbf{J}_2^T\mathbf{y} \\
\end{aligned}
\right.
$$

由第二式得

$$
\mathbf{x}_2 = (\mathbf{J}_2^T\mathbf{J}_2)^{-1}(\mathbf{J}_2^T\mathbf{y}-\mathbf{J}_2^T\mathbf{J}_1\mathbf{x}_1 )
$$

代入第一式得

$$
\mathbf{J}_1^T\mathbf{J}_1\mathbf{x}_1 + \mathbf{J}_1^T\mathbf{J}_2(\mathbf{J}_2^T\mathbf{J}_2)^{-1}(\mathbf{J}_2^T\mathbf{y}-\mathbf{J}_2^T\mathbf{J}_1\mathbf{x}_1)=\mathbf{J}_1^T\mathbf{y}
$$

解得

$$
\mathbf{x}_1 = (\mathbf{J}_1^T\mathbf{J}_1 - \mathbf{J}_1^T\mathbf{J}_2(\mathbf{J}_2^T\mathbf{J}_2)^{-1}\mathbf{J}_2^T\mathbf{J}_1)^{-1}(\mathbf{J}_1^T\mathbf{y}-\mathbf{J}_1^T\mathbf{J}_2(\mathbf{J}_2^T\mathbf{J}_2)^{-1}\mathbf{J}_2^T\mathbf{y})
$$

所以法方程的解为

$$
\left\lbrace
\begin{aligned}
  \mathbf{x}_1 &= (\mathbf{J}_1^T\mathbf{J}_1 - \mathbf{J}_1^T\mathbf{J}_2(\mathbf{J}_2^T\mathbf{J}_2)^{-1}\mathbf{J}_2^T\mathbf{J}_1)^{-1}(\mathbf{J}_1^T\mathbf{y}-\mathbf{J}_1^T\mathbf{J}_2(\mathbf{J}_2^T\mathbf{J}_2)^{-1}\mathbf{J}_2^T\mathbf{y}) \\
  \mathbf{x}_2 &= (\mathbf{J}_2^T\mathbf{J}_2)^{-1}(\mathbf{J}_2^T\mathbf{y}-\mathbf{J}_2^T\mathbf{J}_1\mathbf{x}_1 )
\end{aligned}
\right.
$$

在计算时，应先解出像片的外方位元素，再依次计算各地面点坐标值。

[目录](./../../README.md)
