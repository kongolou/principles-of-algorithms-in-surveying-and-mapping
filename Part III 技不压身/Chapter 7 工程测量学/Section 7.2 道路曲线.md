在道路工程中，常进行道路曲线的测设工作。圆曲线是一种常见的道路曲线。

![道路圆曲线示意图](Fig7.4.png)

若转向角 $\theta$ 和圆曲线半径 $R$ 已知，则圆曲线可解。

$$
\begin{aligned}
  T &= R\tan{\dfrac{\theta}{2}} \\
  L &= R\theta \\
  q &= 2T-L
\end{aligned}
$$

其中，$T$ 为切线长， $L$ 为曲线长， $q$ 为切曲差。

解出圆曲线后，再根据 $N$ 点的里程 $K_N$ ，求出圆曲线上各主点的里程。

$$
\begin{aligned}
  K_P &= K_N - T \\
  K_M &= K_N - T + \frac{L}{2} \\
  K_Q &= K_N - T + L
\end{aligned}
$$

检核条件为 $K_Q \equiv K_N + T - q$ 。

在圆曲线上放样里程桩常采用极坐标法。例如，每隔一段距离 $S$ 放样一点，则每次放样结束后的转向角为 $\alpha=\dfrac{S}{R}$ 。

# 回旋线

曲率随弧长线性变化的曲线。
$$
K \propto L
$$
因为 $K=\dfrac{1}{R}$ ，不妨设 $R \cdot L=A^2$ 。
则有
$$
\left\lbrace
\begin{aligned}
 dx &= dL\cos{d\beta} \\
 dy &= dL\sin{d\beta}
\end{aligned}
\right.
$$
其中
$$
d\beta=\frac{dL}{R}=\frac{L}{A^2}dL
$$
不定积分得
$$
\beta=\int{d\beta}=\int{\frac{L}{A^2}dL}=\frac{L^2}{2A^2}+C
$$
起点处曲率为零，由此确定
$$
\beta=\frac{L^2}{2A^2}
$$

沿缓曲线积分
$$
\left\lbrace
\begin{aligned}
 x(L) &= \int_0^{L_S}\cos{\frac{L^2}{2A^2}}dL \\
 y(L) &= \int_0^{L_S}\sin{\frac{L^2}{2A^2}}dL
\end{aligned}
\right.
$$

![](Fig7.5.png)

## 回旋线数值计算公式

$$
\left\lbrace
\begin{aligned}
 x(L) &= L-\frac{L^3}{40R^2}+\frac{L^5}{3456R^4}-\frac{L^7}{599040R^6} \\
 y(L) &= \frac{L^2}{6R}-\frac{L^4}{336R^3}+\frac{L^6}{42240R^5}-\frac{L^8}{9676800R^7}
\end{aligned}
\right.
$$

[目录](./../../README.md)
