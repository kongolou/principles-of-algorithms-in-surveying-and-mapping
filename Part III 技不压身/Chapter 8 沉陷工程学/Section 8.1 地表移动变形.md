在地下煤层开采的过程中，地表会发生一定程度的移动变形。其主要形式有地表移动盆地、裂缝与台阶、塌陷坑等。

# 描述地表移动变形的指标

![地表移动变形](Fig8.1.png)

## 下沉

$$
\begin{aligned}
  W_2 &= -\Delta H_{22'}=H_2-H_{2'} \\
  W_3 &= -\Delta H_{33'}=H_3-H_{3'} \\
  W_4 &= -\Delta H_{44'}=H_4-H_{4'}
\end{aligned}
$$

## 倾斜

$$
\begin{aligned}
  i_{23} &= \frac{\Delta W_{23}}{L_{23}}=\frac{W_3-W_2}{x_3-x_2} \\
  i_{34} &= \frac{\Delta W_{34}}{L_{34}}=\frac{W_4-W_3}{x_4-x_3}
\end{aligned}
$$

## 曲率

$$
K_{234}=\frac{\Delta i_{234}}{\dfrac{1}{2}(L_{23}+L_{34})}=\frac{i_{34}-i_{23}}{\dfrac{1}{2}(x_4-x_2)}
$$

特别地，如果 $L_{23}=L_{34}=L$ ，则有

$$
K_{234}=\frac{\Delta i_{234}}{L}
$$

## 水平移动

$$
\begin{aligned}
  U_2 &= \Delta x_{22'}=x_{2'}-x_2 \\
  U_3 &= \Delta x_{33'}=x_{3'}-x_3 \\
  U_4 &= \Delta x_{44'}=x_{4'}-x_4
\end{aligned}
$$

## 水平变形

$$
\begin{aligned}
  \varepsilon_{23} &= \frac{\Delta U_{23}}{L_{23}}=\frac{U_3-U_2}{x_3-x_2} \\
  \varepsilon_{34} &= \frac{\Delta U_{34}}{L_{34}}=\frac{U_4-U_3}{x_4-x_3}
\end{aligned}
$$

正值为拉伸变形，负值为压缩变形。

# 圈定边界的角值参数

圈定边界的角值参数包括边界角、移动角、裂缝角等。三者均通过测量得到。

## 边界角

在充分或接近充分采动的条件下，地表移动盆地主断面上盆地边界点至同侧采空区边界点的连线与水平线在煤柱一侧的夹角。

$$
\begin{array}{c}
  \hline
  下山 & 上山 & 走向 & 底板 \\
  \hline
  \beta_0 & \gamma_0 & \delta_0 & \lambda_0 \\
  \hline
\end{array}
$$

## 移动角

在充分或接近充分采动的条件下，地表移动盆地主断面上移动边界点至同侧采空区边界点的连线与水平线在煤柱一侧的夹角。

$$
\begin{array}{c}
  \hline
  下山 & 上山 & 走向 & 底板 & 松散层 \\
  \hline
  \beta & \gamma & \delta & \lambda & \phi \\
  \hline
\end{array}
$$

## 裂缝角

在充分或接近充分采动的条件下，地表移动盆地主断面上最外侧的地表裂缝至同侧采空区边界点的连线与水平线在煤柱一侧的夹角。

$$
\begin{array}{c}
  \hline
  下山 & 上山 & 走向 & 底板 \\
  \hline
  \beta'' & \gamma'' & \delta'' & \lambda'' \\
  \hline
\end{array}
$$

# 其他角值参数

在描述地表移动变形的过程中，我们还要用到其他的角值参数。如最大下沉角、充分采动角、开采影响传播角、主要影响角、超前影响角、最大下沉速度滞后角等。其中，最大下沉角通过测量得到，其他角度通过计算得到。

## 最大下沉角

在倾斜主断面上，地表移动盆地最大下沉位置在地表水平线上的投影点至采空区中点的连线与水平线在下山方向的夹角。

$$
\begin{array}{c}
  \hline
  最大下沉角 \\
  \hline
  \theta \\
  \hline
\end{array}
$$

## 充分采动角

在充分采动条件下，地表移动盆地的主断面上盆地平底边缘在地表水平线上的投影点至同侧采空区边界点的连线与煤层在采空区一侧的夹角。

$$
\begin{array}{c}
  \hline
  下山 & 上山 & 走向 \\
  \hline
  \psi_1 & \psi_2 & \psi_3 \\
  \hline
\end{array}
$$

## 开采影响传播角

在倾向主断面上，地表移动盆地拐点位置在地表水平线上的投影点至同侧采空区计算边界的连线与水平线在下山方向的夹角。

$$\theta_0=\arctan\frac{W_0}{U_{W_0}}$$

## 主要影响角

开采深度与主要影响半径的反正切值。

$$\beta=\arctan\frac{H}{r}$$

## 超前影响角

开采深度与超前影响距的反正切值。

$$\omega=\arctan\frac{H_0}{l}$$

## 最大下沉速度滞后角

平均开采深度与最大下沉速度滞后距的反正切值。

$$\Phi=\arctan\frac{H_0}{L}$$

[目录](./../../README.md)
