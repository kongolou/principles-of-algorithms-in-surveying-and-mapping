下面将介绍经典的概率积分法开采沉陷预计过程。

# 概率积分法预计参数

$$
\begin{aligned}
  &下沉系数& q &= \frac{W_0}{m\cos\alpha} \\
  &主要影响角正切& \tan\beta &= \frac{H}{r} \\
  &拐点偏移距& s_0 &= 0.05\sim0.3H \\
  &水平移动系数& b &= \frac{U_0}{W_0} \\
  &开采影响传播角& \theta_0 &= \arctan\frac{W_0}{U_{W_0}}
\end{aligned}
$$

# 半无限开采

## 走向主断面

在走向主断面上，倾斜曲线与水平移动曲线相似、曲率曲线与水平变形曲线相似。

### 地表移动与变形值预计公式

$$
\begin{aligned}
  W_0 &= mq\cos\alpha \\
  W(x) &= \frac{W_0}{2}(erf(\frac{\sqrt{\pi}}{r}x)+1) \\
  i(x) &= \frac{W_0}{r}e^{-\pi\frac{x^2}{r^2}} \\
  K(x) &= -\frac{2\pi W_0}{r^3}xe^{-\pi\frac{x^2}{r^2}} \\
  U(x) &= bri(x) = bW_0e^{-\pi\frac{x^2}{r^2}} \\
  \varepsilon(x) &= brK(x) = -\frac{2\pi bW_0}{r^2}xe^{-\pi\frac{x^2}{r^2}}
\end{aligned}
$$

### 地表移动与变形最大值计算

$$
\begin{aligned}
  W_0 &= mq\cos\alpha \\
  i_0 &= \frac{W_0}{r} \\
  K_0 &= \pm1.52\frac{W_0}{r^2} \\
  U_0 &= bW_0 \\
  \varepsilon_0 &= \pm1.52\frac{bW_0}{r}
\end{aligned}
$$

### 任一点地表移动与变形值计算

$$
\begin{aligned}
  A(\frac{x}{r}) &= \frac{W_x}{W_0} = \frac{1}{2}(erf(\frac{\sqrt{\pi}}{r}x)+1) \\
  A'(\frac{x}{r}) &= \frac{i_x}{i_0} = \frac{U_x}{U_0} \\
  A''(\frac{x}{r}) &= \frac{K_x}{K_0} = \frac{\varepsilon_x}{\varepsilon_0}
\end{aligned}
$$

## 倾向主断面

在倾向主断面上，各曲线不再相似。

### 地表移动与变形值预计公式

$$
\begin{aligned}
  W_0 &= mq\cos\alpha \\
  W(y) &= \frac{W_0}{2}(erf(\frac{\sqrt{\pi}}{r}y)+1) \\
  i(y) &= \frac{W_0}{r}e^{-\pi\frac{y^2}{r^2}} \\
  K(y) &= -\frac{2\pi W_0}{r^3}ye^{-\pi\frac{y^2}{r^2}} \\
  U(y) &= U_0e^{-\pi\frac{y^2}{r^2}}\pm W(y)\cot\theta_0 \\
  \varepsilon(y) &= -\frac{2\pi U_0}{r^2}ye^{-\pi\frac{y^2}{r^2}}\pm i(y)\cot\theta_0
\end{aligned}
$$

# 有限开采

## 仅走向主断面未充分采动

$$
\begin{aligned}
  l &= D-s_3-s_4 \\
  W^0(x) &= W(x)-W(x-l) \\
  i^0(x) &= i(x)-i(x-l) \\
  K^0(x) &= K(x)-K(x-l) \\
  U^0(x) &= U(x)-U(x-l) \\
  \varepsilon^0(x) &= \varepsilon(x)-\varepsilon(x-l)
\end{aligned}
$$

## 仅倾向主断面未充分采动

$$
\begin{aligned}
  L &= (D-s_1-s_2)\frac{\sin(\theta_0+\alpha)}{\sin\theta_0} \\
  W^0(y) &= W_1(y)-W_2(y-L) \\
  i^0(y) &= i_1(y)-i_2(y-L) \\
  K^0(y) &= K_1(y)-K_2(y-L) \\
  U^0(y) &= U_1(y)-U_2(y-L) \\
  \varepsilon^0(y) &= \varepsilon_1(y)-\varepsilon_2(y-L)
\end{aligned}
$$

## 走向与倾向主断面均未充分采动

### 走向主断面

沿走向主断面，引入倾向采动系数 $C_y=\dfrac{W_y^0}{W_0}$ 。

$$
\begin{aligned}
  W^0(x) &= C_y(W(x)-W(x-l)) \\
  i^0(x) &= C_y(i(x)-i(x-l)) \\
  K^0(x) &= C_y(K(x)-K(x-l)) \\
  U^0(x) &= C_y(U(x)-U(x-l)) \\
  \varepsilon^0(x) &= C_y(\varepsilon(x)-\varepsilon(x-l))
\end{aligned}
$$

### 倾向主断面

沿倾向主断面，引入走向采动系数 $C_x=\dfrac{W_x^0}{W_0}$ 。

$$
\begin{aligned}
  W^0(y) &= C_x(W_1(y)-W_2(y-L)) \\
  i^0(y) &= C_x(i_1(y)-i_2(y-L)) \\
  K^0(y) &= C_x(K_1(y)-K_2(y-L)) \\
  U^0(y) &= C_x(U_1(y)-U_2(y-L)) \\
  \varepsilon^0(y) &= C_x(\varepsilon_1(y)-\varepsilon_2(y-L))
\end{aligned}
$$

[目录](./../../README.md)
