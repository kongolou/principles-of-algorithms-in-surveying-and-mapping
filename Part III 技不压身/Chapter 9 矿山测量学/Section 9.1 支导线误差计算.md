在井下作业时，通常不具备良好的观测条件，支导线成为平面测量的首要形式。

![支导线示意图](Fig9.1.png)

如图所示，支导线上任意点的误差计算过程即将该点视作支导线终点的误差计算过程。

支导线终点 K 的坐标通过坐标正算得到。

$$
\left\lbrace
\begin{aligned}
 x_k &= x_0 + \sum_{j=0}^{k-1}S_j\cos\alpha_j \\
 y_k &= y_0 + \sum_{j=0}^{k-1}S_j\sin\alpha_j \\
 \alpha_j &= \alpha_0 \pm \sum_{i=1}^{j}\beta_i + C_j
\end{aligned}
\right.
$$

在井下测量工作中，受到地面点投点误差的影响，地下支导线的起算边将存在误差。即

$$
\left\lbrace
\begin{aligned}
  x_k &= x_k(x_0,S_0,\cdots,S_{k-1},\alpha_0,\beta_1,\cdots,\beta_{k-1}) \\
  y_k &= y_k(y_0,S_0,\cdots,S_{k-1},\alpha_0,\beta_1,\cdots,\beta_{k-1})
\end{aligned}
\right.
$$

视各误差项独立，那么根据协方差传播定律，得到支导线终点误差为

$$
\left\lbrace
\begin{aligned}
  \sigma_{x_k}^2 &= \mathbf{J}_{x_k}\textrm{diag}(\sigma_{x_0}^2,\sigma_{S_0}^2,\cdots,\sigma_{S_{k-1}}^2,\sigma_{\alpha_0}^2,\sigma_{\beta_1}^2,\cdots,\sigma_{\beta_{k-1}}^2)\mathbf{J}_{x_k}^T \\
  \sigma_{y_k}^2 &= \mathbf{J}_{y_k}\textrm{diag}(\sigma_{y_0}^2,\sigma_{S_0}^2,\cdots,\sigma_{S_{k-1}}^2,\sigma_{\alpha_0}^2,\sigma_{\beta_1}^2,\cdots,\sigma_{\beta_{k-1}}^2)\mathbf{J}_{y_k}^T
\end{aligned}
\right.
$$

其中，

$$
\left\lbrace
\begin{aligned}
  \mathbf{J}_{x_k} &= \left[\frac{\partial{x_k}}{\partial{x_0}},\frac{\partial{x_k}}{\partial{S_0}},\cdots,\frac{\partial{x_k}}{\partial{S_{k-1}}},\frac{\partial{x_k}}{\partial{\alpha_0}},\frac{\partial{x_k}}{\partial{\beta_1}},\cdots,\frac{\partial{x_k}}{\partial{\beta_{k-1}}}\right] \\
  \mathbf{J}_{y_k} &= \left[\frac{\partial{y_k}}{\partial{y_0}},\frac{\partial{y_k}}{\partial{S_0}},\cdots,\frac{\partial{y_k}}{\partial{S_{k-1}}},\frac{\partial{y_k}}{\partial{\alpha_0}},\frac{\partial{y_k}}{\partial{\beta_1}},\cdots,\frac{\partial{y_k}}{\partial{\beta_{k-1}}}\right]
\end{aligned}
\right.
$$

以 $\mathbf{J}_{x_k}$ 为例

$$
\begin{aligned}
  \frac{\partial{x_k}}{\partial{x_0}} &= 1 \\
  \frac{\partial{x_k}}{\partial{S_j}} &= \cos\alpha_j \\
  \frac{\partial{x_k}}{\partial{\alpha_0}} &= \sum_{j=0}^{k-1}\frac{\partial{x_k}}{\partial{\alpha_j}}\frac{\partial{\alpha_j}}{\partial{\alpha_0}} = \sum_{j=0}^{k-1}S_j(-\sin\alpha_j) \cdot 1 \\
  &= -\sum_{j=0}^{k-1}S_j\sin\alpha_j = -\textrm{proj}(\vec{r}_{OK},\vec{y}) \\
  \frac{\partial{x_k}}{\partial{\beta_i}} &= \sum_{j=0}^{k-1}\frac{\partial{x_k}}{\partial{\alpha_j}}\frac{\partial{\alpha_j}}{\partial{\beta_i}} = \sum_{j=i}^{k-1}S_j(-\sin\alpha_j) \cdot \pm{1} \\
  &= \mp\sum_{j=i}^{k-1}S_j\sin\alpha_j = \mp\textrm{proj}(\vec{r}_{IK},\vec{y})
\end{aligned}
$$

解得

$$
\sigma_{x_k}^2=\sigma_{x_0}^2+\sum_{j=0}^{k-1}\sigma_{S_j}^2\cos^2\alpha_j+\sigma_{\alpha_0}^2\textrm{proj}(\vec{r}_{OK},\vec{y})^2+\sum_{i=1}^{k-1}\sigma_{\beta_i}^2\textrm{proj}(\vec{r}_{IK},\vec{y})^2
$$

同理解得

$$
\sigma_{y_k}^2=\sigma_{y_0}^2+\sum_{j=0}^{k-1}\sigma_{S_j}^2\sin^2\alpha_j+\sigma_{\alpha_0}^2\textrm{proj}(\vec{r}_{OK},\vec{x})^2+\sum_{i=1}^{k-1}\sigma_{\beta_i}^2\textrm{proj}(\vec{r}_{IK},\vec{x})^2
$$

可以看到，随着路线的延长，终点的误差将会越来越大。当其大到起算边的误差远小于测角和量边的误差时，起算边的误差就可以忽略不计。此时若采用钢尺量边和独立等精度测角，则边角观测的误差值可通过下式计算。

$$
\left\lbrace
\begin{aligned}
  \sigma_{S_j}^2 &= a^2S_j+b^2S_j^2 \\
  \sigma_{\beta_i}^2 &= m_{\beta}^2
\end{aligned}
\right.
$$ 
其中， $a$ 、 $b$ 、 $m_\beta$ 为已知值。

[目录](./../../README.md)
