在上一节讲到，支导线终点误差会随着支导线的延长而不断增大，但是，支导线中不存在多余观测，我们无法对其进行平差。所以，每当支导线延长到一定长度后，就有必要在其末尾再加测一条陀螺定向边来构成方向附合导线，从而进行平差计算。

不过，方向附合导线只能对角度进行平差，而无法对距离进行平差。但这就已经足够了，因为在艰难的地下环境中，方向比距离更重要。

陀螺定向是一种特殊的测量方式，它可以直接测得目标边的坐标方位角。在方向附合导线中，终点边的坐标方位角将成为唯一的多余观测条件。

$$
\alpha_k=\alpha_0\pm\sum_{i=1}^{k}\beta_i+C_k
$$

将 $\alpha_k$ 视作观测值， $\beta_i$ 视作参数值。则两者构成显函数关系。得

$$
\left\lbrace
\begin{aligned}
  \mathbf{J_{L_X}} &= \begin{bmatrix} \pm{1} & \cdots & \pm{1} \end{bmatrix} \\
  \mathbf{y} &= (\alpha_k) - \Big( \alpha_0 \pm \sum_{i=1}^{k}(\beta_i) + C_k \Big) \\
  \mathbf{x} &= (\mathbf{J_{L_X}}^T\mathbf{P}^T\mathbf{J_{L_X}})^{-1}\mathbf{J_{L_X}}^T\mathbf{P}^T\mathbf{y}
\end{aligned}
\right.
$$

如果采用独立等精度测角，即 $\mathbf{P}=\mathbf{I}_k$ ，那么最终将解得

$$
x_i=\frac{y}{k}
$$

改正各方位角得到

$$
\alpha_j=\alpha_0\pm\sum_{i=1}^j\Big((\beta_i)+x_i\Big)+C_j
$$

同样以 $\mathbf{J}_{x_k}$ 为例，与支导线相比，方向附合导线中其他三种雅可比系数的计算方式保持不变，唯独关于测角的雅可比系数的计算方式变为

$$
\begin{aligned}
  \frac{\partial{x_k}}{\partial{\beta_i}} &= \sum_{j=0}^{k-1}\frac{\partial{x_k}}{\partial{\alpha_j}}\frac{\partial{\alpha_j}}{\partial{\beta_i}} = \sum_{j=i}^{k-1}S_j(-\sin\alpha_j) \cdot \pm(1-\frac{1}{k}) \\
  &= \mp(1-\frac{1}{k})\sum_{j=i}^{k-1}S_j\sin\alpha_j = \mp(1-\frac{1}{k})\textrm{proj}(\vec{r}_{IK},\vec{y})
\end{aligned}
$$

可以解得方向附合导线上任意点的点位误差计算公式如下。

$$
\left\lbrace
\begin{aligned}
  \sigma_{x_k}^2 &= \sigma_{x_0}^2+\sum_{j=0}^{k-1}\sigma_{S_j}^2\cos^2\alpha_j+\sigma_{\alpha_0}^2\textrm{proj}(\vec{r}_{OK},\vec{y})^2+\sum_{i=1}^{k-1}\sigma_{\beta_i}^2(1-\frac{1}{k})^2\textrm{proj}(\vec{r}_{IK},\vec{y})^2 \\
  \sigma_{y_k}^2 &= \sigma_{y_0}^2+\sum_{j=0}^{k-1}\sigma_{S_j}^2\sin^2\alpha_j+\sigma_{\alpha_0}^2\textrm{proj}(\vec{r}_{OK},\vec{x})^2+\sum_{i=1}^{k-1}\sigma_{\beta_i}^2(1-\frac{1}{k})^2\textrm{proj}(\vec{r}_{IK},\vec{x})^2
\end{aligned}
\right.
$$

[目录](./../../README.md)
