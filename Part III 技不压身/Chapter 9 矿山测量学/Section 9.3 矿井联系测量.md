将地面的坐标系统传递到井下需要进行联系测量。联系测量分为高程联系测量与平面联系测量，平面联系测量又分为一井定向和两井定向。

# 高程联系测量

如图，矿井的高程联系测量是一个简单的加减法问题。

$$
h=L+b-a
$$

![高程传递](Fig9.2.png)

# 平面联系测量

将井上的方向边通过井筒投影到井下时，投点误差将会产生投向误差。

$$\theta=\dfrac{e}{c}$$

下图所展示的是 $e$ 取最大值时的情况。

![投向误差](Fig9.3.png)

## 一井定向

一井定向常采用连接三角形完成。如图，连接三角形是一种狭长的三角形，施测时要求 $\gamma_1$ 、 $\gamma_4$ 、 $S_{12}$ 、 $S_{34}$ 的值都尽可能小，而 $c$ 的值尽可能大，同时整个联系测量部分要尽可能短。

![方向传递](Fig9.4.png)

在连接三角形中， $\beta_2$ 与 $\beta_3$ 无法直接观测，需要通过其他观测值计算得到。

由正弦定理，解得

$$
\left\lbrace
\begin{aligned}
  \beta_2 &= 360\degree - \frac{S_{13}}{c}\gamma_1 \\
  \beta_3 &= \frac{S_{24}}{c}\gamma_4
\end{aligned}
\right.
$$

则 $\alpha_4$ 可通过下式计算

$$
\alpha_4=\alpha_0+\beta_1+\beta_2+\beta_3+\beta_4\pm\theta+C \\
$$

其中， $\theta$ 为投点误差引起的投向误差。

独立等精度测角时，根据协方差传播定律有

$$
\sigma_{\alpha_4}^2=\sigma_{\alpha_0}^2+\sigma_{\beta_1}^2+\sigma_{\beta_2}^2+\sigma_{\beta_3}^2+\sigma_{\beta_4}^2+
\sigma_\theta^2
$$

其中，

$$
\left\lbrace
\begin{aligned}
  \sigma_{\beta_2}^2 &= \frac{S_{13}^2}{c^2}\sigma_{\gamma_{1}}^2 \\
  \sigma_{\beta_3}^2 &= \frac{S_{24}^2}{c^2}\sigma_{\gamma_{4}}^2 \\
  \sigma_{\theta}^2 &= \frac{e^2}{c^2} \\
\end{aligned}
\right.
$$

而 $\sigma_{\beta_1}^2=\sigma_{\beta_4}^2=\sigma_{\gamma_1}^2=\sigma_{\gamma_4}^2=m_{\beta}^2$ 。

## 两井定向

在两井定向时，由于两井相距较远，投点间距 $c$ 较大，因此由投点误差产生的投向误差 $\theta$ 可以忽略不计。这时的方向误差主要来源于井上下平面联系测量部分。

设两井代号为 M 、 N 。则 MN 法向 $\vec{\eta}$ 为重要方向。由井上下平面联系测量部分因坐标点位误差引起的方向误差为

$$
\sigma_{\alpha_{MN}}^2=\frac{1}{c^2}(\sigma_{\eta_{M_{上}}}^2+\sigma_{\eta_{N_{上}}}^2+\sigma_{\eta_{M_{下}}}^2+\sigma_{\eta_{N_{下}}}^2)
$$

其中，井上下部分的点位误差可采用支导线误差计算公式计算。

[目录](./../../README.md)
