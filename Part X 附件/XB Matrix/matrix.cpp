
#include "matrix.h"
#include <cassert>
#include <cmath>

void Matrix::create() {
  m_data = new double *[m_rows];
  for (unsigned int i = 0u; i < m_rows; ++i) {
    m_data[i] = new double[m_cols];
  }
}

void Matrix::destroy() const {
  for (unsigned int i = 0u; i < m_rows; ++i) {
    delete[] m_data[i];
  }
  delete[] m_data;
}

Matrix::Matrix(const unsigned int &rows, const unsigned int &cols)
    : m_rows(rows), m_cols(cols) {
  create();
}

Matrix::Matrix(const Matrix &other) : Matrix(other.m_rows, other.m_cols) {
  for (unsigned int i = 0u; i < m_rows; ++i) {
    for (unsigned int j = 0u; j < m_cols; ++j) {
      m_data[i][j] = other.m_data[i][j];
    }
  }
}

Matrix::Matrix(Matrix &&other)
    : m_rows(other.m_rows), m_cols(other.m_cols), m_data(other.m_data) {
  other.m_rows = 0u;
  other.m_cols = 0u;
  other.m_data = nullptr;
}

Matrix &Matrix::operator=(const Matrix &other) {
  if (this != &other) {
    destroy();
    m_rows = other.m_rows;
    m_cols = other.m_cols;
    create();
    for (unsigned int i = 0u; i < m_rows; ++i) {
      for (unsigned int j = 0u; j < m_cols; ++j) {
        m_data[i][j] = other.m_data[i][j];
      }
    }
  }
  return *this;
}

Matrix &Matrix::operator=(Matrix &&other) {
  if (this != &other) {
    destroy();
    m_rows = other.m_rows;
    m_cols = other.m_cols;
    m_data = other.m_data;
    other.m_rows = 0u;
    other.m_cols = 0u;
    other.m_data = nullptr;
  }
  return *this;
}

Matrix::~Matrix() { destroy(); }

Matrix Matrix::operator+(const Matrix &other) const {
  assert(m_rows == other.m_rows);
  assert(m_cols == other.m_cols);
  Matrix result{m_rows, m_cols};
  for (unsigned int i = 0u; i < result.m_rows; ++i) {
    for (unsigned int j = 0u; j < result.m_cols; ++j) {
      result.m_data[i][j] = m_data[i][j] + other.m_data[i][j];
    }
  }
  return result;
}

Matrix Matrix::operator-(const Matrix &other) const {
  assert(m_rows == other.m_rows);
  assert(m_cols == other.m_cols);
  Matrix result{m_rows, m_cols};
  for (unsigned int i = 0u; i < result.m_rows; ++i) {
    for (unsigned int j = 0u; j < result.m_cols; ++j) {
      result.m_data[i][j] = m_data[i][j] - other.m_data[i][j];
    }
  }
  return result;
}

Matrix Matrix::operator*(const Matrix &other) const {
  assert(m_cols == other.m_rows);
  Matrix result{m_rows, other.m_cols};
  for (unsigned int i = 0u; i < result.m_rows; ++i) {
    for (unsigned int j = 0u; j < result.m_cols; ++j) {
      for (unsigned int k = 0; k < m_cols; ++k) {
        result.m_data[i][j] += m_data[i][k] * other.m_data[k][j];
      }
    }
  }
  return result;
}

Matrix operator*(const double &number, const Matrix &matrix) {
  Matrix result{matrix};
  for (unsigned int i = 0u; i < result.m_rows; ++i) {
    for (unsigned int j = 0u; j < result.m_cols; ++j) {
      result.m_data[i][j] *= number;
    }
  }
  return matrix;
}

unsigned int Matrix::getRows() const { return m_rows; }

unsigned int Matrix::getCols() const { return m_cols; }

double &Matrix::at(const unsigned int &i, const unsigned int &j) const {
  assert(i < m_rows);
  assert(j < m_cols);
  return m_data[i][j];
}

void Matrix::setZero() const {
  for (unsigned int i = 0u; i < m_rows; ++i) {
    for (unsigned int j = 0u; j < m_cols; ++j) {
      m_data[i][j] = 0.0;
    }
  }
}

Matrix Matrix::getTransposedMatrix() const {
  Matrix result{m_cols, m_rows};
  for (unsigned int i = 0u; i < result.m_rows; ++i) {
    for (unsigned int j = 0u; j < result.m_cols; ++j) {
      result.m_data[i][j] = m_data[j][i];
    }
  }
  return result;
}

Matrix Matrix::getSubmatrix(const unsigned int &x,
                            const unsigned int &y) const {
  assert(x < m_rows);
  assert(y < m_cols);
  Matrix result{m_rows - 1, m_cols - 1};
  for (unsigned int i = 0u; i < result.m_rows; ++i) {
    for (unsigned int j = 0u; j < result.m_cols; ++j) {
      result.m_data[i][j] =
          m_data[(i < x) ? i : (i + 1)][(j < y) ? j : (j + 1)];
    }
  }
  return result;
}

SquareMatrix::SquareMatrix(const unsigned int &dimension)
    : Matrix(dimension, dimension) {}

// Get detetminant by Pseudo-LU Decomposition:
// det(LU) = det(L) * det(U) = 1 * prod(diag(U)).
// 1. Let H = I first.
// 2. Then calculate H[i][j] = i < j ? L[i][j] : U[i][j] in turn.
// 3. Finally we get prod(diag(H)) = prod(diag(U)) = det(LU).
double SquareMatrix::getDeterminant() const {
  double result = 1.0;
  unsigned int dimension = getRows();
  SquareMatrix helper{dimension};
  helper.setZero();
  for (unsigned int i = 0; i < dimension; ++i) {
    helper.at(i, i) = 1.0;
  }
  for (unsigned int i = 0; i < dimension; ++i) {
    for (unsigned int j = 0; j < dimension; ++j) {
      double tmp = 0.0;
      for (unsigned int k = 0; k < (i < j ? i : j); ++k) {
        tmp += helper.at(i, k) * helper.at(k, j);
      }
      helper.at(i, j) = (at(i, j) - tmp) / helper.at(j, j);
      if (i == j) {
        result *= helper.at(i, i);
        if (std::abs(result) < 1e-15) {
          return 0.0;
        }
      }
    }
  }
  return result;
}

SquareMatrix SquareMatrix::getAdjointMatrix() const {
  unsigned int dimension = getRows();
  SquareMatrix result{dimension};
  for (unsigned int i = 0u; i < dimension; ++i) {
    for (unsigned int j = 0u; j < dimension; ++j) {
      double det = matrix_to_square_matrix(getSubmatrix(i, j)).getDeterminant();
      result.at(j, i) = (((i + j) % 2 == 0) ? 1 : -1) * det;
    }
  }
  return result;
}

SquareMatrix SquareMatrix::getInverseMatrix() const {
  double det = getDeterminant();
  assert(!(std::abs(det) < 1e-15));
  return matrix_to_square_matrix(1.0 / det * getAdjointMatrix());
}

SquareMatrix matrix_to_square_matrix(Matrix matrix) {
  assert(matrix.getRows() == matrix.getCols());
  SquareMatrix origin;
  Matrix *tmp = &origin;
  *tmp = matrix;
  SquareMatrix *result = dynamic_cast<SquareMatrix *>(tmp);
  return *result;
}
