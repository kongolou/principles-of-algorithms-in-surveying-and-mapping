
#ifndef MATRIX_H
#define MATRIX_H

class Matrix {
private:
  unsigned int m_rows = 0u;
  unsigned int m_cols = 0u;
  double **m_data = nullptr;

protected:
  virtual void create() final;
  virtual void destroy() const final;

public:
  Matrix() = default;
  Matrix(const unsigned int &rows, const unsigned int &cols);
  Matrix(const Matrix &other);
  Matrix(Matrix &&other);
  virtual ~Matrix();
  Matrix &operator=(const Matrix &other);
  Matrix &operator=(Matrix &&other);
  Matrix operator+(const Matrix &other) const;
  Matrix operator-(const Matrix &other) const;
  Matrix operator*(const Matrix &other) const;
  friend Matrix operator*(const double &number, const Matrix &matrix);
  unsigned int getRows() const;
  unsigned int getCols() const;
  double &at(const unsigned int &i, const unsigned int &j) const;
  void setZero() const;
  Matrix getTransposedMatrix() const;
  Matrix getSubmatrix(const unsigned int &i, const unsigned int &j) const;
};

class SquareMatrix : public Matrix {
public:
  SquareMatrix() = default;
  SquareMatrix(const unsigned int &dimension);
  virtual ~SquareMatrix() = default;
  double getDeterminant() const;
  SquareMatrix getAdjointMatrix() const;
  SquareMatrix getInverseMatrix() const;
};

SquareMatrix matrix_to_square_matrix(Matrix matrix);

#endif // MATRIX_H
