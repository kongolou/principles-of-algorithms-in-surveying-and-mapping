
#include "mainwindow.h"
#include "./ui_mainwindow.h"
#include <QDate>
#include <QDir>
#include <QFile>
#include <QFileDialog>
#include <QMessageBox>
#include <QRandomGenerator>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow) {
  ui->setupUi(this);
  connect(ui->action_read_menu, SIGNAL(triggered(bool)), this,
          SLOT(read_menu()));
  connect(ui->action_write_log, SIGNAL(triggered(bool)), this,
          SLOT(write_log()));
  connect(ui->action_get_lucky, SIGNAL(triggered(bool)), this,
          SLOT(get_lucky()));
  connect(ui->action_about_me, SIGNAL(triggered(bool)), this, SLOT(about_me()));
  connect(ui->pushButton_choose, SIGNAL(clicked(bool)), this,
          SLOT(get_lucky()));
}

MainWindow::~MainWindow() { delete ui; }

QList<QString> MainWindow::dishes;

void MainWindow::read_menu() {
  QString here = QDir::currentPath();
  QString menu_path = QFileDialog::getOpenFileName(
      this, tr("Select menu"), here, tr("Text Files (*.txt)"));
  if (menu_path.isEmpty()) {
    QMessageBox msg;
    msg.setText(tr("读取失败."));
    msg.exec();
    return;
  }
  QFile menu(menu_path);
  if (!menu.open(QIODevice::ReadOnly | QIODevice::Text)) {
    return;
  }
  MainWindow::dishes.clear();
  QTextStream in(&menu);
  while (!in.atEnd()) {
    QString line = in.readLine();
    MainWindow::dishes.push_back(line);
  }
  ui->action_get_lucky->setEnabled(true);
  ui->pushButton_choose->setEnabled(true);
  ui->action_write_log->setEnabled(true);
  ui->label_dinner->setText(tr("?"));
}

void MainWindow::write_log() {
  QString here = QDir::currentPath();
  QString log_path = QFileDialog::getOpenFileName(this, tr("Save log"), here,
                                                  tr("Text Files (*.txt)"));
  if (log_path.isEmpty()) {
    QMessageBox msg;
    msg.setText(tr("写入失败."));
    msg.exec();
    return;
  }
  QFile log(log_path);
  if (!log.open(QIODevice::ReadWrite | QIODevice::Text | QIODevice::Append)) {
    return;
  }
  QTextStream out(&log);
  out << QDate::currentDate().toString() << tr("  晚上吃了  ")
      << ui->label_dinner->text() << "\n";
  QMessageBox msg;
  msg.setText(tr("写好了!"));
  msg.exec();
}

void MainWindow::get_lucky() {
  if (dishes.isEmpty()) {
    return;
  }
  auto rnd = QRandomGenerator::global()->generate();
  auto idx = rnd % dishes.size();
  ui->label_dinner->setText(dishes[idx]);
}

void MainWindow::about_me() {
  QMessageBox msg;
  msg.setText(tr("今晚吃啥 by 一个吃货"));
  msg.exec();
}
