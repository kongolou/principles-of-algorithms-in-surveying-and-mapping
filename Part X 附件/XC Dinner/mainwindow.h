
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow

{
  Q_OBJECT

public:
  MainWindow(QWidget *parent = nullptr);
  ~MainWindow();
  static QList<QString> dishes;

private slots:
  void read_menu();
  void write_log();
  void get_lucky();
  void about_me();

private:
  Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
