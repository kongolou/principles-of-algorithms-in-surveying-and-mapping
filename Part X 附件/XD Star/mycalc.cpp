
#include "mycalc.h"
#include <cmath>

double mycalc::mod(const double &a, const double &b) {
  return a - b * floor(a / b);
}

double mycalc::calc_alpha(const double &alpha, const double &beta,
                          const bool &is_left) noexcept {
  double res = alpha;
  if (is_left) {
    res += beta;
  } else {
    res -= beta;
  }
  res -= M_PI;
  return mod(res, 2 * M_PI);
}

double mycalc::calc_alpha(const double &x1, const double &y1, const double &x2,
                          const double &y2) noexcept {
  double dx = x2 - x1;
  double dy = y2 - y1;
  double res = 0.0;
  if (dx > 0.0) {
    res = mod(atan(dy / dx), 2 * M_PI);
  } else if (dx < 0.0) {
    res = atan(dy / dx) + M_PI;
  } else if (dy > 0.0) {
    res = M_PI / 2;
  } else if (dy < 0.0) {
    res = 3 * M_PI / 2;
  } else {
    ;
  }
  return res;
}

double mycalc::calc_dist(const double &x1, const double &y1, const double &x2,
                         const double &y2) noexcept {
  return hypot(x2 - x1, y2 - y1);
}

double mycalc::calc_x(const double &x0, const double &dist,
                      const double &alpha0) noexcept {
  return x0 + dist * cos(alpha0);
}

double mycalc::calc_y(const double &y0, const double &dist,
                      const double &alpha0) noexcept {
  return y0 + dist * sin(alpha0);
}

double mycalc::dms2r(const int &deg, const int &min, const int &sec) noexcept {
  return (deg * 3600.0 + min * 60.0 + sec) / (180.0 * 3600.0) * M_PI;
}

void mycalc::r2dms(const double &rad, int dms[3]) noexcept {
  dms[0] = int(rad / 3600.0);
  dms[1] = int((rad - dms[0] * 3600.0) / 60.0);
  dms[2] = int(rad - dms[0] * 3600.0 - dms[1] * 60.0);
}
