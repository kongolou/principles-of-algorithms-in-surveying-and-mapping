
#ifndef MYCALC_H
#define MYCALC_H

namespace mycalc {
double mod(const double &a, const double &b);
double calc_alpha(const double &alpha, const double &beta,
                  const bool &is_left = true) noexcept;
double calc_alpha(const double &x1, const double &y1, const double &x2,
                  const double &y2) noexcept;
double calc_dist(const double &x1, const double &y1, const double &x2,
                 const double &y2) noexcept;
double calc_x(const double &x0, const double &dist,
              const double &alpha0) noexcept;
double calc_y(const double &y0, const double &dist,
              const double &alpha0) noexcept;
double dms2r(const int &deg, const int &min, const int &sec) noexcept;
void r2dms(const double &rad, int dms[3]) noexcept;
} // namespace mycalc

#endif // MYCALC_H
