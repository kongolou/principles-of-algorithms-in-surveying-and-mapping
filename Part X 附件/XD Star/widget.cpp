
#include "widget.h"
#include "./ui_widget.h"
#include "mycalc.h"
#include <QMessageBox>
#include <QPainter>

Widget::Widget(QWidget *parent) : QWidget(parent), ui(new Ui::Widget) {
  ui->setupUi(this);
}

Widget::~Widget() { delete ui; }

QList<double> Widget::calc_betas(const QList<int> &degrees) {
  QList<double> betas;
  for (auto deg : degrees) {
    betas.push_back(mycalc::dms2r(deg, 0, 0));
  }
  return betas;
}

QList<double> Widget::calc_alphas(const double &alpha0,
                                  const QList<double> &betas) {
  QList<double> alphas;
  alphas.push_back(alpha0);
  auto beta = betas.begin();
  for (int i = 0; beta != betas.end(); ++i, ++beta) {
    alphas.push_back(mycalc::calc_alpha(alphas[i], *beta));
  }
  return alphas;
}

QList<double> Widget::calc_xs(const double &x0, const QList<double> &dists,
                              const QList<double> &alphas) {
  QList<double> xs;
  double xn = x0;
  auto d = dists.begin();
  auto a = alphas.begin();
  for (int i = 0; d != dists.end() && a != alphas.end(); ++i, ++d, ++a) {
    xs.push_back(mycalc::calc_x(xn, *d, *a));
    xn = xs[i];
  }
  return xs;
}

QList<double> Widget::calc_ys(const double &y0, const QList<double> &dists,
                              const QList<double> &alphas) {
  QList<double> ys;
  double yn = y0;
  auto d = dists.begin();
  auto a = alphas.begin();
  for (int i = 0; d != dists.end() && a != alphas.end(); ++i, ++d, ++a) {
    ys.push_back(mycalc::calc_y(yn, *d, *a));
    yn = ys[i];
  }
  return ys;
}

QList<QPointF> Widget::get_star_points(const double &x0, const double &y0) {

  QList<int> degrees{8, 7, 1, 7, 1, 7, 1, 7, 1};
  for (int i = 0; i < degrees.size(); ++i) {
    degrees[i] *= 36;
  }
  QList<double> betas = calc_betas(degrees);

  double alpha0 = 0.0;
  QList<double> alphas = calc_alphas(alpha0, betas);

  double dist = ui->label_star->width() / 3.0;
  QList<double> dists(alphas.size(), dist);
  dists[0] *= 1.5;

  double dy = dist / 5.0;
  QList<double> xs = calc_xs(x0, dists, alphas);
  QList<double> ys = calc_ys(y0 + dy, dists, alphas);

  QList<QPointF> points;
  auto x = xs.begin();
  auto y = ys.begin();
  for (; x != xs.end() && y != ys.end(); ++x, ++y) {
    points.push_back(QPointF(*x, *y));
  }
  return points;
}

void Widget::on_pushButton_draw_star_clicked() {
  QPixmap pix(300, 300);
  pix.fill();
  QPainter pnt(&pix);
  QColor red{200, 0, 0};
  pnt.setPen(red);
  pnt.setBrush(red);
  QPolygonF plg{get_star_points()};
  pnt.drawPolygon(plg);
  ui->label_star->setPixmap(pix);
  ui->pushButton_save_star->setEnabled(true);
}

void Widget::on_pushButton_save_star_clicked() {
  QPixmap pix(ui->label_star->pixmap());
  if (pix.save("star.png", "PNG")) {
    QMessageBox msg(this);
    msg.setText(tr("保存成功!"));
    msg.exec();
  }
}
