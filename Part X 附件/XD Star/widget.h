
#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>

QT_BEGIN_NAMESPACE
namespace Ui {
class Widget;
}
QT_END_NAMESPACE

class Widget : public QWidget

{
  Q_OBJECT

public:
  Widget(QWidget *parent = nullptr);
  ~Widget();

  QList<double> calc_betas(const QList<int> &degrees);
  QList<double> calc_alphas(const double &alpha0, const QList<double> &betas);
  QList<double> calc_xs(const double &x0, const QList<double> &dists,
                        const QList<double> &alphas);
  QList<double> calc_ys(const double &y0, const QList<double> &dists,
                        const QList<double> &alphas);
  QList<QPointF> get_star_points(const double &x0 = 0.0,
                                 const double &y0 = 0.0);

private slots:
  void on_pushButton_draw_star_clicked();
  void on_pushButton_save_star_clicked();

private:
  Ui::Widget *ui;
};

#endif // WIDGET_H
